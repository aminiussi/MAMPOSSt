#! /bin/csh -f

# script to compile and run MAMPOSSt with COSMMOC.
# author: Gary Mamon

set compile = 1
set run = 1
set numchains = 6
set debug = 0
set clean = 1
set inidir = .
set dir_start = `pwd`
set mpich = 1
set iap = 0
set rootdir = ~/MAMPOSSt_CosmoMC

setenv FORTRAN gfortran
# set version = ""

if (`which ifort |& grep -vc 'Command not found'` == 1) then
	echo setenv MPICH_FC ifort ...
	setenv MPICH_FC ifort
	setenv FORTRAN ifort
else if (`which gfortran |& grep -vc 'Command not found'` == 1) then
	setenv MPICH_FC gfortran
else
	exec echo cannot find advanced Fortran compiler
endif

while ($#argv > 0)
	switch ($1)
	case -gfortran:
	case -ifort:
	case -f90:
		setenv FORTRAN `echo $1 | cut -c 2-`
		shift
		breaksw
	case -fc:
		shift
		setenv FORTRAN $1
		shift
		breaksw
	case -nocomp:
		set compile = 0
		shift
		breaksw
	case -noclean:
		set clean = 0
		shift
		breaksw
	case -norun:
		set run = 0
		shift
		breaksw
	case -n:
	case -nchains:
	case -nprocs:
	case -np:
		shift
		set numchains = $1
		shift
		breaksw
	case -help:
	case -h:
		echo "Usage: $0 [options] prefix"
		echo "Options:"
		echo "-d dir	--> root directory for MAMPOSSt distribution"
		echo "-fc comp	--> use comp compiler (default $MPICH_FC)"
		echo "-gfortran	--> use gfortran compiler"
		echo "-ifort		--> use ifort compiler"
		echo "-h or -help	--> display this message"
		echo "-np num		--> run num chains on num cores (default $numchains)"
		echo "-noclean	--> do not recompile camb utilities"
		echo "-nocomp		--> do not compile"
		echo "-norun		--> do not run code"
		echo "-debug lev	--> use debugging level lev (default $debug)"
		exit
	case -debug:
		set debug = 1
		shift
		breaksw
	case -d:
	case -dir:
	case -rootdir:
		shift
		set rootdir = $1
		shift
		breaksw
	case -*:
		exec echo $0:t\: cannot recognize $1
	default:
		if ($?prefix == 0) then
			set prefix = $1
		else
			set inidir = $1
		endif
		shift
		breaksw
	endsw
end

echo prefix = $prefix directory is now `pwd`

# count number of parameters

if (! -r $prefix.ini) then
	exec echo $0:t\: cannot open $prefix.ini
endif

set numhard = `grep -c 'param\[' $prefix.ini`
if ($numhard == "") then
	exec echo $0:t\: cannot obtain number of hard parameters!
endif
echo $prefix has $numhard parameters

set execname = cosmomc_mamposst_nh${numhard}_`hostname`

# MPI compiler and run commands
# This has to be adapated to your computer system!

# if ($f90 == gfortran) then
# 	set mpich = 0
# endif
# if ($mpich) then
# 	set mpiruncmd = mpirun
# 	set f90comp = mpif90
# 	setenv I_MPI_F90 ifort
# 	setenv MPICH_F90 ifort	# probably not necessary!
# else
# 	set mpiruncmd = mpirun
# 	set f90comp = mpif90
# endif

if (`which mpif90.mpich |& grep -vc 'Command not found'` == 1) then
	set f90comp = mpif90.mpich
else if (`which mpif90 |& grep -vc 'Command not found'` == 1) then
	set f90comp = mpif90
else
	set f90comp = $FORTRAN
endif

if (`which mpirun.mpich |& grep -vc 'Command not found'` == 1) then
	set mpiruncmd = mpirun.mpich
	if (`which ifort |& grep -vc 'Command not found'` == 1 && $FORTRAN == ifort) then
		setenv I_MPI_F90 ifort
		setenv MPICH_F90 ifort	# probably not necessary!
	else
		setenv MPICH_F90 gfortran	# probably not necessary!
	endif
else
	if (`which mpirun |& grep -vc 'Command not found'` == 1) then
		set mpiruncmd = mpirun
	else
		echo no MPI command found to run MAMPOSSt
		echo will compile and run in single processor mode
	endif
endif

if (`which mpif90 |& grep -vc 'Command not found'` == 0) then
	exec echo $0:t\: cannot find mpif90... install mpich on your system
endif

# # patch for IAP

# if (`domainname` == iap.fr && $FORTRAN == ifort) then
# 	setenv MPIF90_MPICH ~gam/script/mpif90.mpich
# else
# 	setenv MPIF90_MPICH `where mpif90.mpich | grep usr | tail -1`
# endif

if ($compile) then
	# compile

	# first create numhard.i if necessary

	cd $rootdir/src/COSMOMC
	set write_numhard = 0
	if (! -r numhard.i) then
		set write_numhard = 1
	else
		set old_numhard = `awk '/num_hard/ { print $NF}' numhard.i`
		if ($old_numhard != $numhard) then
			set write_num_hard = 1
		endif
	endif
	if ($write_numhard) then
		cat > numhard.i <<EOF
	integer, parameter :: num_hard = $numhard
EOF
	endif

	# move to src directory and start compilation
	
	cd ..
	if ($clean) then
		make cleanall
	endif
	if ($debug) then
		echo clean = $clean
		echo make EXEC=$rootdir/bin/$execname F90C=$f90comp FORTRAN=$FORTRAN ...
	endif
	touch $rootdir/lib/lib_$FORTRAN/libutils.a
	echo dir = `pwd` ...
	echo make LIBDIR=$rootdir/lib EXEC=$rootdir/bin/$execname F90C=$f90comp FORTRAN=$FORTRAN ...
	make LIBDIR=$rootdir/lib EXEC=$rootdir/bin/$execname F90C=$f90comp FORTRAN=$FORTRAN #  >& ~/NOSAVE/COSMOMC/$prefix.output
else
	touch ~/NOSAVE/COSMOMC/$prefix.output
endif 

cd $dir_start

# run

if ($run) then
	# echo about to launch from `pwd` ...
	# echo "$mpiruncmd -np $numchains $rootdir/bin/$execname $prefix.ini >& ~/NOSAVE/COSMOMC/$prefix.output & ..	."
	nohup $mpiruncmd -np $numchains $rootdir/bin/$execname $prefix.ini # >>& ~/NOSAVE/COSMOMC/$prefix.output &
	set pid = `ps uxww | grep mpirun | grep -v grep | awk '{print $2}'`
	echo code is running\; can be killed with \"kill $pid\"
endif
