.. _mamposst_params:

.. |br| raw:: html

   <br />


MAMPOSSt arguments
==================

* Data

  
:``rr(ndata)``:
	projected radius vector (*R*) in kpc [size ``ndata``]
:``avz(ndata)``:
	absolute line-of-sight velocity vector (\| *v*:sub:`LOS` \|) in km/s (assuming mean near 0) [size ndata]
:``evz(ndata)``:
   	error on line-of-sight velocity in km/s [size ``ndata``]
:``comp(ndata)``:
   	observed component of tracer (1_all for single component, else 1_str1, 2_str2 ...)
:``mu(ndata)``:
   	distance modulus (:math:`<=` ``0`` for none)
:``emu(ndata)``:
	error on distance modulus (``-1`` for none)
:``ndata``:
	number of data points

* Data parameters (vectors of ``ncomp`` length)

:``rrmindata(ncomp)``:
	minimum projected radius (already computed from data to save time)
:``rrmaxdata(ncomp)``:
	maximum projected radius (already computed from data to save time)
:``rrminallow(ncomp)``:
	minimum allowed projected radius (:math:`<` ``0`` for innermost data point)
:``rrmaxallow(ncomp)``:
	maximum allowed projected radius (:math:`<=` ``0`` for outermost data point)
:``avzmaxdata(ncomp)``:
	maximum absolute LOS velocity (already computed from data to save time)
:``avzmaxallow(ncomp)``:
	maximum allowed absolute LOS velocity (:math:`<=` ``0`` for largest data point)
:``mu0``:
	distance modulus of full system if known (ignored if no mu provided
	in data file or if all mu :math:`<` ``0``)

* Tracer structure (vectors of ncomp length)

:``tracermodel(ncomp)``: model of visible tracer; available models are: |br|
	``Hernquist``: :math:`\rho \propto r^{-1}(r+a)^{-3}` (`Hernquist 1990
	<http://adsabs.harvard.edu/abs/1990ApJ...356..359H>`_ model,
	:math:`r_{-2}/a=1/2`) |br|
	``mHubble``: :math:`\rho \propto (r^2+a^2)^{-3/2}` (modified Hubble
	or non-truncated analytical `King 1962
	<http://adsabs.harvard.edu/abs/1962AJ.....67..471K>`_ model,
	:math:`r_{-2}/a=\sqrt{2}`) |br|
	``isothermal``: :math:`\rho \propto (r^2+a^2)^{-1}`
	(pseudo-isothermal) |br|
	``Jaffe``: :math:`\rho \propto r^{-2}(r+a)^{-2}` (`Jaffe 1983
	<http://adsabs.harvard.edu/abs/1983MNRAS.202..995J>`_) |br|
	``NFW``: :math:`\rho \propto r^{-1}(r+a)^{-2}` (`Navarro, Frenk &
	White 1996 <http://adsabs.harvard.edu/abs/1996ApJ...462..563N>`_
	model, :math:`r_{-2}/a=1`) |br|
	``Plummer``: :math:`\rho \propto (r^2+a^2)^{-5/2}` (`Plummer 1911
	<http://adsabs.harvard.edu/abs/1911MNRAS..71..460P>`_ model,
	:math:`r_{-2}/a=\sqrt{2/3}`) |br|
	``gPlummer``: :math:`\rho \propto r^\gamma (r^2+a^2)^{-5/2-\gamma/2}`
	(generalized Plummer with free inner slope :math:`\gamma`,
	:math:`r_{-2}/a=\sqrt{(2+\gamma)/3}`) |br|
	``PrugnielSimien``: :math:`\displaystyle \rho \propto x^{-p(n)}\,\exp
	\left[-b(n)\,\left({r\over R_{\rm eff}}\right)^{1/n}\right]`
	(`Prugniel & Simien 1997
	<http://adsabs.harvard.edu/abs/1997A%26A...321..111P>`_ approximation 
	to deprojected Sersic, where :math:`b(n)` is from analytical
	approximation of `Ciotti & Bertin 1999
	<http://adsabs.harvard.edu/abs/1999A%26A...352..447C>`_, while
	:math:`p(n)` is given by `Lima Neto, Gerbal & Marquez 1999
	<http://adsabs.harvard.edu/abs/1999MNRAS.309..481L>`_)
:``ltracerradius(ncomp)``: :math:`\log_{10}` scale radius of visible tracer (kpc)
:``meanltracerradius(ncomp)``: mean (best) externally derived log(tracer
			       radius), :math:`<` ``0``  for internally derived
:``sigltracerradius(ncomp)``: error in externally derived log(tracer radius),
			      :math:`<` ``0`` for internally derived
:``tracerpar2(ncomp)``:   additional tracer parameter
:``ltracermass(ncomp)``:  :math:`\log_{10}` tracer mass in M_solar at rfid
:``ltracermasstot``:      :math:`\log_{10}` total tracer mass in M_solar at rfid
:``fractracer(ncomp)``:   fraction of total tracer mass in tracer
:``rfidtracer(ncomp)``:   fiducial radius for tracer mass or  ``0`` for infinity, or  :math:`<` ``0`` for 10^ltracerradius (kpc)

* Tracer velocity anisotropy (vectors of ncomp length)

:``anismodel(ncomp)``: 	velocity anisotropy model(s) *model1* etc. of visible
			tracer(s); available models are: |br| 
	``iso``: :math:`\beta = 0` (isotropic) |br|
	``cst``: :math:`\beta` = constant |br|
	``ML``: :math:`\displaystyle \beta = {1\over 2} {r\over r+r_\beta}`
	(`Mamon & Łokas 2005b <http://adsabs.harvard.edu/abs/2005MNRAS.363..705M>`_) |br|
	``OM``: :math:`\displaystyle \beta = {r^2\over r^2+r_\beta^2}`
	(`Osipkov 1979 <http://adsabs.harvard.edu/abs/1979SvAL....5...42O>`_;
	`Merritt 1985 <http://adsabs.harvard.edu/abs/1985MNRAS.214P..25M>`_)
	|br|
	``gOM``: :math:`\displaystyle \beta = \beta_0 +
	(\beta_\infty-\beta_0)\,{r^2\
	r^2+r_\beta^2}` (generalized Osipkov-Merritt) |br|
	``Tiret``: :math:`\displaystyle \beta = \beta_0 +
	(\beta_\infty-\beta_0)\,{r\over r+r_\beta}` (`Tiret et al. 2007
	<http://adsabs.harvard.edu/abs/2007A%26A...476L...1T>`_)
:``anisflag``:         ``0`` :math:`\displaystyle \to \log_{10} \left({\sigma_r\over \sigma_\theta}\right)\qquad`
		       ``1`` :math:`\to \beta\qquad`
		       ``2`` :math:`\displaystyle \to \beta_{\rm sym} =
		       {\sigma_r^2-\sigma_\theta^2 \over \sigma_r^2+\sigma_\theta^2}`
:``lanis0(ncomp)``:     central velocity anisotropy (see ``anisflag``)
:``lanisinf(ncomp)``:   outer velocity anisotropy (see ``anisflag``)
:``lanisradius(ncomp)``: :math:`\log_{10}` anisotropy radius (kpc) (irrelevant for 'isotropic')

:``ncomp``:             number of components

* Dark Matter

:``darkmodel``:    model of dark matter; available models are: |br|
		``Burkert``: :math:`\rho \propto (r+a)^{-1}(r^2+a^2)^{-1}` (`Burkert
		1995 <http://adsabs.harvard.edu/abs/1995ApJ...447L..25B>`_
		model, :math:`r_{-2}/a=(1-\sqrt{26/27})^{1/3}+(1+\sqrt{26/27})^{1/3})` |br|
		``Einasto``: :math:`\rho \propto \exp[-b(n)\, r^{1/n}]`
		(`Einasto 1965
		<http://adsabs.harvard.edu/abs/1965TrAlm...5...87E>`_ model,
		:math:`r_{-2}/a = (2n)^n`) |br|
		``Hernquist``: :math:`\rho \propto r^{-1}(r+a)^{-3}`
		(`Hernquist 1990
		<http://adsabs.harvard.edu/abs/1990ApJ...356..359H>`_ model, :math:`r_{-2}/a=1/2`) |br|
		``gHernquist``: :math:`\rho \propto r^\gamma
		(r+a)^{-4-\gamma}` (generalized Hernquist model with free
		inner slope :math:`\gamma`, :math:`r_{-2}/a=1+\gamma/2`) |br|
		``mHubble``: :math:`\rho \propto (r^2+a^2)^{-3/2}` (modified
		Hubble or non-truncated analytical `King 1962
		<http://adsabs.harvard.edu/abs/1962AJ.....67..471K>`_ model, :math:`r_{-2}/a=\sqrt{2}`) |br|
		``isothermal``: :math:`\rho \propto (r^2+a^2)^{-1}`
		(pseudo-isothermal) |br|
		``Jaffe``: :math:`\rho \propto r^{-2}(r+a)^{-2}` (`Jaffe 1983
		<http://adsabs.harvard.edu/abs/1983MNRAS.202..995J>`_) |br|
		``Kazantzidis``: :math:`\rho \propto r^\gamma \exp(-r/a)`
		(`Kazantzidis et al. 2004
		<http://adsabs.harvard.edu/abs/2004ApJ...608..663K>`_ model, :math:`r_{-2}/a=2+\gamma`) |br|
		``NFW``: :math:`\rho \propto r^{-1}(r+a)^{-2}` (`Navarro,
		Frenk & White 1996
		<http://adsabs.harvard.edu/abs/1996ApJ...462..563N>`_ model, :math:`r_{-2}/a=1`) |br|
		``cNFW``: :math:`\rho \propto (r+a)^{-3}` (cored NFW model, :math:`r_{-2}/a=2`)
		|br|
		``gNFW``: :math:`\rho \propto r^\gamma (r+a)^{-3-\gamma}`
		(generalized NFW model with free inner slope, :math:`r_{-2}/a=2+\gamma`) |br|
		``Plummer``: :math:`\rho \propto (r^2+a^2)^{-5/2}` (`Plummer
		1911
		<http://adsabs.harvard.edu/abs/1911MNRAS..71..460P>`_ model,
		:math:`r_{-2}/a=\sqrt{2/3}`) |br| 
		``gPlummer``: :math:`\rho \propto r^\gamma
		(r^2+a^2)^{-5/2-\gamma/2}` (generalized Plummer with free
		inner slope :math:`\gamma`, :math:`r_{-2}/a=\sqrt{(2+\gamma)/3}`) |br|
:``norm``:         normalization of dark matter (mass or radius, see darknormflag)
:``darknormflag``: flag for dark matter or total normalization ``norm``: |br|
                 ``-1``: :math:`\log_{10} r_{\rm vir}` (kpc) |br|
                 ``0``:  :math:`\log_{10} M_{\rm vir}` (:math:`\rm M_\odot`) |br|
                 :math:`>` ``0``: :math:`\log_{10} M(r_{\rm fid}^{\rm dark}` =
		 ``darknormflag``) in  :math:`\rm M_\odot`
:``darkscale``:        :math:`\log_{10}` scale of dark matter (scale radius [generally
		       :math:`r_{-2}`] or concentration :math:`r_{\rm vir}/r_{\rm scale}`)
:``darkscaleflag``:    flag for ``darkscale`` -> ``1``: scale radius, ``2``:
		       concentration (:math:`r_{\rm vir}/r_{\rm scale}`)
:``darktotflag``:
	``1``: dark
   	``2``: total (``norm`` then concerns total normalization)
:``darkpar2``:         additional dark matter parameter

* Central Black Hole

:``lbhmass``:      :math:`\log_{10}` black hole mass (:math:`\rm M_\odot`)

* 3D velocity model

:``v3dmodel``:     model of 3D velocities (Gauss for now)

* Cosmology

:``Delta``:        mean overdensity at virial radius relative to critical density of Universe
:``h``:            dimensionless Hubble constant  :math:`H_0` / (100 km/s/Mpc)
:``Omegam``:       density parameter at  :math:`z=0`
:``z``:            redshift of object

* Other parameters

:``rmax``:         maximum LOS integration radius (kpc)
:``MfLflag``:      [Mass-follows-Light]    ``1`` -> force a_dark=a_tracer, tracermodel=darkmodel
:``TLMflag``:      [Tied-Light-Mass]       ``1`` -> force a_tracer=a_dark, tracermodel=darkmodel
:``TALflag``:	[Tied-Anisotropy-radius-Light]  ``1`` -> force a_anis=a_tracer

:``a0lclM``:       normalization of log(concentration) vs log(halo-mass) relation
:``a1lclM``:       slope of log(concentration) vs log(halo-mass) relation

:``splitpflag``:   ``1`` -> determine a_tracer separately to gain time (less accurate)
                   if ltracerradius  :math:`>=` ``9``, solve for tracerradius and exit
:``wt``:           weights [array of size ndata]
:``distflag``:     for data with distance modulus (mu): 
                   ``0``: ignore mu, ``1``: Gaussian(mu) weight, ``2``: Gaussian(mu) * density weight
:``lBilop``:        :math:`\log_{10} B` for interlopers (virial units)
:``ilopflag``:     ``0`` -> halo only, ``1`` -> standard to infinity (with interlopers), ``2`` ->
		   halo + interlopers jointly

:``debug``:        ``0``: no debug output, ``1``: lnL, ``2``: verbose, ``3``:
		   more, ``4``: verbose

