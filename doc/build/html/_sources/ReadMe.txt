What is MAMPOSSt?
=================
To be written!

Installation
============
To be written!

MAMPOSSt parameter choices
==========================
To be written!

MAMPOSSt publications
=====================
The basic article on MAMPOSSt is `Mamon, Biviano & Bou�; 2013, MNRAS 429,
3079 <http://adsabs.harvard.edu/abs/2013MNRAS.429.3079M>`_

Running MAMPOSSt
================

Generalities
------------
MAMPOSSt is run using the `COSMOMC <http://cosmologist.info/cosmomc/readme.html>`_ MCMC program by `Anthony Lewis <http://cosmologist.info>`_, which uses the `Metropolis-Hastings <https://en.wikipedia.org/wiki/Metropolis-Hastings_algorithm>`_ algorithm to probe the multi-dimensional parameter space. COSMOMC is usually run using several *chains* probing the parameter space in parallel. COSMOMC was developed to analyze the CMB, but for MAMPOSSt it is compiled and run in ``generic_MCMC`` mode. 

Modifications to COSMOMC
------
Several COSMOMC routines were modified for MAMPOSSt: 

* ``driver.F90`` --> ``driver_mamposst.F90`` (the main program for reading parameter file)
* ``settings.f90`` --> ``settings_mamposst.f90`` (for declarations)
* ``params_CMB.f90`` --> ``params_mamposst.f90`` (for reading the file  of parameter names, automatically generated by the code)
* ``calclike.f90`` --> ``calclike_mamposst.f90`` (to read the data and call the MAMPOSSt likelihood function)
* ``readmamposst.f90`` file was added to read the specific MAMPOSSt parameter for tracers with multiple components.

Compilation
------------

	``make -f Makefile[version]``

where the current working version is 40. 

Running
----
	``mpirun -np num_chains cosmomc param_initialization_file``

where ``num_chains`` is the number of chains in parallel. If you run on an 8-core
machine, you should specify :math:`{\tt num\_chains} \leq 8`. 

Initialization file
----
The COSMOMC initialization file has been now tailored for MAMPOSSt. 

Scripts to speed things up
==========================

All C-shell scripts below accept options ``-h`` and ``-help`` for help.

* ``buildini_mamposst [options] name``:
	* converts data-file to MAMPOSSt format;
	* generates initialization file ``params_mamposst_prefix.ini``;
	* chains will be called ``mamposst_prefix_n.txt`` where ``n`` is the chain number, starting at 1;
	* on exit, proposes the ``autoruncosmomc`` script (see below) with the precise arguments or simply a run-command, in both cases saving the output (for debugging) into an appropriate file.

* ``rebuildini [options] old-name new-name`` [to be re-coded]: 
same as ``buildini`` but using specific initialization file as default.

* ``autoruncosmomc [-v version] [-norun] prefix``: 
automatic script for compilation and optional running.

* ``launchcosmomc`` (to be completed)

* ``getcosmomcrun dir [prefix]``: 
script to copy chain outputs from remote to local machine.





