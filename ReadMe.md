This is the git repository for `MAMPOSSt`, a code to extract the parameters of the radial profiles of mass and velocity from the projected phase space (projected radii and line-of-sight velocities) of spherically-assumed clusters. 

* Documentation

  is [HERE](http://www.iap.fr/users/gam/MAMPOSSt/doc/build/html/index.html)

* To install:

  % INSTALL.csh

(where `%' is the UNIX prompt)

* To run test program (INSTALL.csh should do this for you, but if you want to
  try again without installing)

  % cd test
  % buildini_mamposst -data_dir . -data_file test.dat -n 2 Passive SF -lrtr 2.5 3.4 2.9 3.8 test

* To plot test results (assuming you have SM)

  % sm
  sm> mosaicMCMC test 10000

(where `sm>' is the SM prompt)

gv (`open' on Mac) test.pdf



