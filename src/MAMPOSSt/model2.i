      real*8 atracer, adark, bet0, betinf, anisrad, rad
      real*8 rcube, rfiddark, rmaxx, rhs, darkparam2
      real*8 Mtraceratatr11, Mdarkatad11, MBH11
      real*8 fourpideltarhocover3, rmaxdata, tracerparam2
      real*8 Mfid11
      real*8 param2, rvir, gam, mu00, aa0lclM, aa1lclM
      integer distflagg
      character*16 tracermod, darkmod, anismod, v3dmod
      common /MODEL/ atracer, adark, tracermod, darkmod, 
     &  anismod, tracerparam2, 
     &  bet0, betinf, anisrad,
     &  rcube, rfiddark, Mtraceratatr11, Mdarkatad11, Mfid11,
     &  rmaxx, rad, MBH11, rhs, fourpideltarhocover3, rmaxdata, 
     &  darkparam2, rvir, gam, aa0lclM, aa1lclM,
     &	v3dmod, mu00, distflagg
c
c
c
