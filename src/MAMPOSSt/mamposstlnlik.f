      double precision function mamposstlnlik(rr,avz,evz,comp,mu,emu,
     &  ndata,
     &  rrmindata,rrmaxdata,rrminallow,rrmaxallow,
     &  avzmaxdata,avzmaxallow,mu0,
     &  tracermodel,
     &  ltracerradius,meanltracerradius,sigltracerradius,tracerpar2,
     &  ltracermass,ltracermasstot,fractracer,rfidtracer,
     &  anismodel,anisflag,lanis0,lanisinf,lanisradius,ncomp,
     &  darkmodel,norm,darknormflag,darkscale,darkscaleflag,
     &  darktotflag,darkpar2,
     &  lbhmass,v3dmodel,
     &  Delta,h,Omegam,z,
     &  rmax,MfLflag,TLMflag,TALflag,a0lclM,a1lclM,siglc,splitpflag,
     &  wt,distflag,lBilop,ilopflag,
     &  debug)
c
c MAMPOSSTLNLIK returns -ln Likelihoods given model and parameters 
c for a given data set
c
c --- Gary Mamon, July 2010, last revised Feb 2018
c
c This version handles multiple tracer components for joint MLE
c AND expects to see a 6-column data file finishing with distance modulus (mu) and its error (emu)
c in the 5th and 6th columns, respectively.
c
c Revisions:
c []: original version (see MBB 13) 
c 2: multiple components
c     25: ...
c 3.2 (15 Oct 2014): scale radii or r_{-2} or R_e when possible
c
c 3.3 (17 Oct 2014): new arguments: ltracermasstot and fractracer (ncomp or really ncomp-1)
c
c 3.4 (17 Oct 2014): replaced gNFW approximation by direct calculation via Hypergeometric2F1
c
c 3.5 (19 Oct 2014): fixed bug in gNFW mass profile
c
c 3.6 (15 Feb 2015): added c-M with r_fid
c
c 3.7 (13 May 2016): added ln L_0(r_nu) or ln Gaussian to ln likelihood in case of splitpflag
c     added arguments meanltracerradius and sigltracerradius (external priors on log r_nu)
c     removed ninterp argument 
c
c 3.8  (7 Jun 2016): added anisflag, anisflag=0: log (sigma_r/sigma_theta), anisflag=1: beta
c
c 3.9  (14 Oct 2016): added anisflag=2 for symmetrized beta
c
c 4.0: (4 Nov 2016): replaced proprietary software by public versions (mainly on John Burkardt's Web page)
c       integrator by public (and 3x faster) DQAGS [SLATEC]
c       zero-solver by modified ZERO [BRENT]
c       cubic spline coeffs by SPLINE_CUBIC_SET [SPLINE]
c       cubic spline interpolation by SPLINE_CUBIC_VAL [SPLINE]
c       ln(Gamma(a)) by DGAMLN [SLATEC]
c       incomplete gamma function by DGAMI [SLATEC]
c                --> crashes sometimes!
c
c 5.0: (18 Feb 2018): replaced DQAGS by DGAUS8 [SLATEC too]. Tested.
c
c 5.1: (7 Dec 2018): added power-law model, betasym=22, and extra arg siglc (scatter on c-M)
c
c arguments:
c ---------
c
cccc Data
c
c rr(ndata):    projected radius vector (R) in kpc [size ndata]
c avz(ndata):   absolute line-of-sight velocity vector (|v_z|) in km/s (assuming mean near 0) [size ndata]
c evz(ndata):   error on line-of-sight velocity [size ndata]
c comp(ndata):  observed component of tracer (1_all for single component, else 1_str1, 2_str2 ...)
c mu(ndata):           distance modulus (<=0 for none)
c emu(ndata):          error on distance modulus (-1 for none)
c ndata:        number of data points
c
cccc Data (vectors of ncomp length)
c
c rrmindata(ncomp):    minimum projected radius (already computed from data to save time)
c rrmaxdata(ncomp):    maximum projected radius (already computed from data to save time)
c rrminallow(ncomp):   minimum allowed projected radius (< 0 for innermost data point)
c rrmaxallow(ncomp):   maximum allowed projected radius (<= 0 for outermost data point)
c avzmaxdata(ncomp):   maximum absolute LOS velocity (already computed from data to save time)
c avzmaxallow(ncomp):  maximum allowed absolute LOS velocity (<= 0 for largest data point)
c mu0:                 distance modulus of full system (ignored if all mu < 0)
c
cccc Tracer structure (vectors of ncomp length)
c
c tracermodel(ncomp):  model of visible tracer:
c   'Hernquist':                    Hernquist 90
c   'NFW':                          Navarro, Frenk & White 96
c   'Sersic' or 'PrugnielSimien':   Prugniel & Simien deprojection of Sersic
c   'HernquistGen':                 Hernquist with free inner slope
c   'Jaffe':                        HernquistGen with inner slope -2
c   'Plummer':                      Plummer 1911
c   'PlummerGen'                    Plummer with general inner slope
c   'King' or 'ModHubble'           Modified Hubble (analytical King 62 with no truncation)
c   'powerlaw'                      Power law
c ltracerradius(ncomp): log scale radius of visible tracer (kpc)
c meanltracerradius(ncomp): mean (best) externally derived log(tracer radius) [new in 3.7!]
c sigltracerradius(ncomp): error in externally derived log(tracer radius), < 0 for internally derived [NEW in 3.7!]
c tracerpar2(ncomp):   additional tracer parameter
c ltracermass(ncomp):  log tracer mass in M_solar at rfid
c ltracermasstot:      log total tracer mass in M_solar at rfid
c fractracer(ncomp):   fraction of total tracer mass in tracer
c rfidtracer(ncomp):   fiducial radius for tracer mass or 0 for infinity, or < 0 for 10^ltracerradius (kpc)
c
cccc Tracer velocity anisotropy (vectors of ncomp length)
c
c anismodel(ncomp):  anisotropy model:
c                   'isotropic': isotropic
c                   'cst': beta=constant
c                   'OM': Osipkov-Merritt
c                   'OMgen': generalized Osipkov-Merritt (free beta_0 & beta_inf)
c                   'ML': Mamon-Lokas
c                   'T': Tiret (possibly free beta_0 & beta_inf) 
c anisflag:
c               0    -> log (sig_r/sig_theta),
c               1    -> beta,
c               2    -> betasym = (sig_r^2-sig_th^2)/(sig_r^2+sig_th^2)
c               22   -> betasym = 2 (sig_r^2-sig_th^2)/(sig_r^2+sig_th^2)
c lanis0(ncomp):     log central sig_r/sig_theta or beta_0 or betasym_0
c lanisinf(ncomp):   log outer sig_r/sig_theta or beta_inf or betasym_inf
c lanisradius(ncomp): log anisotropy radius (kpc) (irrelevant for 'isotropic')
c
c ncomp:             number of components
c
cccc Dark Matter
c
c darkmodel:    model of dark matter
c   'Hernquist':                    Hernquist 90
c   'NFW':                          Navarro, Frenk & White 96
c   'Sersic' or 'PrugnielSimien':   Prugniel & Simien deprojection of Sersic
c   'HernquistGen':                 Hernquist with free inner slope
c   'NFWGen':                       NFW with free inner slope
c   'NFWGenout':                    NFW with free outer slope
c   'Zhao':                         Zhao (alpha beta gamma) [not implemented yet?]
c   'Jaffe':                        HernquistGen with inner slope -2
c   'Plummer':                      Plummer 1911
c   'Einasto':                      Einasto (Navarro et al. 04)
c   'Burkert'                       Burkert 95
c   'cNFW' or 'Cubic'               cored NFW, e.g. Trilling et al. in prep.
c norm:         normalization of dark matter (mass or radius, see darknormflag)
c darknormflag: flag for dark matter or total normalization `norm':
c                 -1: log r_vir (kpc)
c                 0:  log M_vir (M_solar)
c                 >0: log M(r_fid^dark = darknormflag) in M_solar
c darkscale:        log scale of dark matter (scale radius [generally r_{-2} or concentration r_vir/r_{-2})
c darkscaleflag:    flag for `darkscale' -> 1: scale radius, 2: concentration (r_vir/r_scale)
c darktotflag:      1: dark, 2: total (`norm' then concerns total normalization)
c darkpar2:         additional dark matter parameter
c
cccc Central Black Hole
c
c lbhmass:      log black hole mass (M_solar)
c
cccc 3D velocity model
c
c v3dmodel:     model of 3D velocities (Gauss, Tsallis1, Tsallis2)
c
cccc Cosmology
c
c Delta:        mean overdensity at virial radius relative to critical density of Universe
c h:            dimensionless Hubble constant H(z) / [100 km/s/Mpc]
c Omegam:       density parameter at z=0
c z:            redshift of object
c
cccc Other parameters
c
c rmax:         maximum LOS integration radius (kpc)
c MfLflag:      [Mass-follows-Light]    1 -> force a_dark=a_tracer, tracermodel=darkmodel
c TLMflag:      [Tied-Light-Mass]       1 -> force a_tracer=a_dark, tracermodel=darkmodel
c TALflag:	[Tied-Anisotropy-radius-Light]  1 -> force a_anis=a_tracer
c
c a0lclM:       normalization of log(concentration) vs log(halo-mass/solar) relation
c a1lclM:       slope of log(concentration) vs log(halo-mass) relation
c siglc         uncertainty on normalization of log(concentration) vs log(halo-mass/solar)
c
c splitpflag:   1 -> determine a_tracer separately to gain time (less accurate)
c                if ltracerradius>=9, solve for tracerradius and exit
c wt:           weights [array of size ndata]
c distflag:     for data with distance modulus (mu): 
c                   0: ignore mu, 1: Gaussian(mu) weight, 2: Gaussian(mu) * density weight
c lBilop:       log B for interlopers (virial units)
c ilopflag:     0 -> g_halo only, 1 -> std to infinity (with Bilop), 2 -> g_halo + g_ilop
c
c debug:        0: no debug output, 1: lnL, 2: verbose, 3: all
c
c
c Minimization method
c -------------------
c
c splitpflag=0 (joint modeling of R,v given known radial incompleteness) 
c   --> ln L = -sum_i w_i ln q(R_i,v_i)
c
c splitpflag=1 AND sigltracerradius(n)>0  (roughly known tracer radius from external data)
c   --> ln L = -sum_n sum_i ln p(v_i|R_i) + [(ltracerradius(n)-meanltracerradius(n))/sigltracerradius(n)]^2/2
c
c splitpflag=1 AND sigltracerradius <= 0  (completeness only applied to distribution of radii)
c   --> ln L = -sum_i ln p(v_i|R_i) - sum_i w_i ln p_0(R_i)
c
      implicit none
c
c argument declarations
c
      integer ndata, MfLflag, TLMflag, splitpflag, ncomp
      integer TALflag, distflag, anisflag
      integer darkscaleflag, darktotflag, ilopflag, debug
      real*8 rr(ndata), avz(ndata), evz(ndata), wt(ndata)
      real*8 mu(ndata), emu(ndata), mu0
      real*8 ltracerradius(ncomp), meanltracerradius(ncomp)
      real*8 sigltracerradius(ncomp)
      real*8 ltracermass(ncomp), ltracermasstot, fractracer(ncomp)
      real*8 lanis0(ncomp), lanisinf(ncomp), lanisradius(ncomp)
      real*8 norm, darknormflag, darkscale, darkpar2
      real*8 lbhmass, tracerpar2(ncomp)
      real*8 rmax, rrminallow(ncomp), rrmaxallow(ncomp)
      real*8 rrmindata(ncomp), rrmaxdata(ncomp)
      real*8 avzmaxallow(ncomp), avzmaxdata(ncomp), rfidtracer(ncomp)
      real*8 Delta, h, Omegam, z, a0lclM, a1lclM, siglc
      real*8 lBilop
      character*16 tracermodel(ncomp), anismodel(ncomp)
      character*16 darkmodel, v3dmodel, comp(ndata)
c
c other declarations
c
      include 'params.i'
      include 'integ.i'
      integer n, ndum, i, j, nrb, nsig, ier, icbcs, maxfn, iatr, nd
      integer nrbdum
      integer ipass, atoi, nrbinsold
      integer limit, lenw, last, neval, iwork(MAXSPLITINTEG)
      integer icdum, kdum
      integer nn, nn2, ok, sumnonzero, icomp
      integer sgn1, found
      real*8 Mvir11, Mdarkatrvir11, lMtracer11, lMvir11, lMvirdark11
      real*8 Mfiddark11, Mdark11, Mtracer11, Mtraceratrvdark11
      real*8 darkradiusmin, darkradiusmax, darkradius, rtracermass
      real*8 rvirdark, c, Mvirdark11, lcmin, lcmax, lrvirmin, lrvirmax
      real*8 Mtraceratrvird11, lMtraceratrvird11, rdark
      real*8 DeltaNprojtildea3, DeltaNproj2, DeltaNprojtilde, vvir
      real*8 DeltaNprojtildesph
      real*8 Nofa, twopiBa3overNofa, piDeltaR2Deltav
      real*8 beta0, betainf
      real*8 gilopfac, Nvir, xx, ctr
      real*8 denom, denomhalo, denomilop
      real*8 denom0, denom0halo, denom0ilop
      real*8 num, numhalo, numilop
      real*8 num0, num0halo, num0ilop
      real*8 lcmean
      real*8 lnrrmin, lnrrmax, dlnrr, davz, lnrmax, lnrmax3
      real*8 sum, sigmar, dlnr, dlnr0
      real*8 latrmin, latrmax, ldarkradiusmax, ldarkradiusmin
      real*8 aerr, rerr, aerror, work(4*MAXSPLITINTEG)
      real*8 tol
      real*8 lnrr(MAXOBJ)
      real*8 lnpinterp(MAXOBJ), lnpfinal(MAXOBJ), lnp0(MAXOBJ)
      real*8 lnpknot(MAXINTERP,MAXINTERP), cbcs(2,MAXINTERP,2,MAXINTERP)
      real*8 lnrrknot(MAXINTERP), avzknot(MAXINTERP)
      real*8 rrknot, rrn, avzn, avzknotmax, gg, Mtraceratrv11, xxn
      real*8 evzn, evz0, mun, emun
c      real*8 lnrdum(2), lsigr(2)
      real*8 sigzR2R, betaprofile
      real*8 gilophat, ghalofac
      real*8 flrv, phimax(1), lnrmax1(1), dens
      real*8 xxmin, xxmax, Iilopapx, ilopfactor, sumfractracer
      real*8 atr, Mtr11, atracerrad(MAXCOMP), lnlik(MAXCOMP)
      real*8 one, Eofz, Hofz, gam3, minusone, par2, p2tr
      real*8 rminus2, rminoveradark, rmaxoveradark
      real*8 Xvir, sigi, Ailop, sigmailop
      real*8 hya, hyb, hyc, xarg
      real*8 fld, fld1
      real*8 vir2code
      real*8 zero, gamma
      character*16 compon, compon2, trmod
      logical isnan
c
c functions called
c
      integer sgn
      real*8 mass, massproj, density, surfdensity, g
      real*8 massprojsph
c
c debugging variables
c
      real*8 gofrvir
c
c common blocks
c
      include 'model2.i'
      include 'csig40.i'
      include 'splineatr.i'
      include 'ilop.i'
      include 'csts.i'
      include 'components.i'
      include 'specfuns.i'
      include 'debug.i'
c
c$$$      real*8 catr2(MAXATRACER,3)
c$$$      real*8 latr2(MAXATRACER), lnlikatr2(MAXATRACER)
      real*8 ldarkradius, fldarkradius, flc, flrvir, lc, lrvir
      integer icsigr2, nrbins2, kdimsigr2
      real*8 lnr2(MAXRBINS), lsigmar2(MAXRBINS)
c     real*8 csigr2(MAXRBINS,3)
c$$$      real*8 wk(MAXRBINS,6)
c$$$      real*8 csigr2(4,MAXRBINS), breaksigr2(MAXRBINS)
c$$$      real*8 phi2(MAXRBINS), cphi2(MAXRBINS,3)
      real*8 ypp2(MAXRBINS)
      real*8 lnr1(1), lsigr1(1), nu, lnr0, sum2
      real*8 third
      character*16 trmodel
c
      real*8 fnusgr2, fnusigr2, lnrdum
      external fldarkradius, flc, flrvir, fnusigr2, fphi, flatr, fNilop
c
c first checks
c
      if (ndata .gt. MAXOBJ) then
         print *,' MAMPOSSTLNLIK: ndata MAXDATA = ', ndata, MAXOBJ
         stop
      endif 
      if (ncomp .gt. MAXCOMP) then
         print *,' MAMPOSSTLNLIK: ncomp MAXCOMP = ', ncomp, MAXCOMP
         stop
      endif 
c
c constants
c
      debugg = debug
      if (debug .eq. 2) then
         print *,' and now in MAMPOSSTLNLIK'
         print *,' 1st 10 data points:'
         do n = 1, 10
            print *, ' R |v_z| err(v_z) comp mu emu w = ', sngl(rr(n)), 
     &        sngl(avz(n)), sngl(evz(n)), comp(n), 
     &        sngl(mu(n)), sngl(emu(n)), sngl(wt(n))
         enddo
      endif
      if (debug .ge. 2) then
         print *,' in MAMPOSSTLNLIK version 4.0 ...'
         write(*,*) ' ndata = ', ndata
         write(*,*) ' ncomp = ', ncomp
         do n = 1, ncomp
            write(*,*) ' '
            write(*,*) ' component ', n, '...'
            write(*,*) ' rrmindata = ', rrmindata(n)
            write(*,*) ' rrmaxdata = ', rrmaxdata(n)
            write(*,*) ' avzmaxdata = ', avzmaxdata(n)
            write(*,*) ' rrminallow = ', rrminallow(n)
            write(*,*) ' rrmaxallow = ', rrmaxallow(n)
            write(*,*) ' avzmaxallow = ', avzmaxallow(n)
            write(*,*) ' tracermodel = ', tracermodel(n)
            write(*,*) ' ltracerradius = ', ltracerradius(n)
            write(*,*) ' sigltracerradius = ', sigltracerradius(n)
            write(*,*) ' rfidtracer = ', rfidtracer(n)
            write(*,*) ' ltracermass = ', ltracermass(n)
            write(*,*) ' fractracer = ', fractracer(n)
            write(*,*) ' tracerpar2 = ', tracerpar2(n)
            write(*,*) ' anismodel = ', anismodel(n)
            write(*,*) ' lanis0 = ', lanis0(n)
            write(*,*) ' lanisinf = ', lanisinf(n)
            write(*,*) ' lanisradius = ', lanisradius(n)
         enddo
         write(*,*) ' '
         write(*,*) ' ltracermasstot = ', ltracermasstot
         write(*,*) ' anisflag = ', anisflag
         write(*,*) ' darkmodel = ', darkmodel
         write(*,*) ' darkscale = ', darkscale
         write(*,*) ' norm = ', norm
         write(*,*) ' darknormflag = ', darknormflag
         write(*,*) ' darkscaleflag = ', darkscaleflag
         write(*,*) ' darktotflag = ', darktotflag
         write(*,*) ' darkpar2 = ', darkpar2
         write(*,*) ' lbhmass = ', lbhmass
         write(*,*) ' v3dmodel = ', v3dmodel
         write(*,*) ' Delta = ', Delta
         write(*,*) ' h = ', h
         write(*,*) ' rmax = ', rmax
         write(*,*) ' mu0 = ', mu0
         write(*,*) ' MfLflag = ', MfLflag
         write(*,*) ' TLMflag = ', TLMflag
         write(*,*) ' TALflag = ', TALflag
         write(*,*) ' splitpflag = ', splitpflag
         write(*,*) ' lBilop = ', lBilop
         write(*,*) ' ilopflag = ', ilopflag
         print *,' rr(1) avz(1) evz(1) wt(1) = ', rr(1), avz(1), evz(1)
     &    , wt(1)
      endif
c
c Flags not yet implemented
c
      if (distflag .eq. 2) then
         print *,' MAMPOSSTLNLIK: distance indicators with priors',
     &    ' not yet implemented'
         stop
      endif
c
c Impossible flag values
c
      if (darkscaleflag .ne. 1 .and. darkscaleflag .ne. 2) then
         print *,' MAMPOSSTLNLIK: darkscaleflag must be 1 or 2'
         stop
      endif  
      if (darktotflag .ne. 1 .and. darktotflag .ne. 2) then
         print *,' MAMPOSSTLNLIK: darktotflag must be 1 or 2'
         stop
      endif  
      if (MfLflag .ne. 0 .and. MfLflag .ne. 1) then
         print *,' MAMPOSSTLNLIK: MfLflag must be 0 or 1'
         stop
      endif
      if (TLMflag .ne. 0 .and. TLMflag .ne. 1) then
         print *,' MAMPOSSTLNLIK: TLMflag must be 0 or 1'
         stop
      endif
      if (TALflag .ne. 0 .and. TALflag .ne. 1) then
         print *,' MAMPOSSTLNLIK: TALflag must be 0 or 1'
         stop
      endif
      if (ilopflag .ne. 0 .and. ilopflag .ne. 1 .and.
     &  ilopflag .ne. 2) then
         print *,' MAMPOSSTLNLIK: ilopflag must be 0, 1 or 2'
         stop
      endif 
c
c Impossible parameter combinations
c
      if (MfLflag .eq. 1 .and. TLMflag .eq. 1) then
         print *,
     &    ' MAMPOSSTLNLIK: cannot set both MfLflag and TLMflag to 1'
         stop
      endif
      if (TLMflag .eq. 1 .and. splitpflag .eq. 1) then
         print *,
     &    ' MAMPOSSTLNLIK: cannot set both TLMflag & splitpflag to 1'
         stop
      endif
      if (darknormflag .gt. SMALL .and. MfLflag .eq. 1) then
         print *,' MAMPOSSTLNLIK cannot set both MfLflag',
     &    ' and darknormflag > 0'
         stop
      endif
      if (darknormflag .gt. SMALL .and. TLMflag .eq. 1) then
         print *,' MAMPOSSTLNLIK cannot set both TLMflag',
     &    ' and darknormflag > 0'
         stop
      endif
c$$$      if (a0lclM .gt. 0.d0 .and. 
c$$$     &  (MfLflag .eq. 1 .or. TLMflag .eq. 1 
c$$$     &  .or. darknormflag .gt. SMALL)) then
c$$$         print *,
c$$$     &   '  MAMPOSSTLNLIK: cannot set a0lclM > 0 (c = c_LCDM(M))',
c$$$     &   ' AND (MfLflag OR TLMflag OR darknormflag > 0)'
c$$$         stop
c$$$      endif
      if (a0lclM .gt. 0.d0 .and. 
     &  (MfLflag .eq. 1 .or. TLMflag .eq. 1)) then
         print *,
     &   '  MAMPOSSTLNLIK: cannot set a0lclM > 0 (c = c_LCDM(M))',
     &   ' AND (MfLflag OR TLMflag)'
         stop
      endif
c
c Translate model names
c
      if (darkmodel .eq. 'nfw') darkmodel = 'NFW'
      if (darkmodel .eq. 'NFWgen' .or. darkmodel .eq. 'nfwgen') then
         darkmodel = 'gNFW'
      end if
      if (darkmodel .eq. 'hernquist') darkmodel = 'Hernquist'
      if (darkmodel .eq. 'Hernquistgen'
     &  .or. darkmodel .eq. 'HernquistGen'
     &  .or. darkmodel .eq. 'hernquistgen') then
         darkmodel = 'gHernquist'
      end if
      if (darkmodel .eq. 'plummer') darkmodel = 'Plummer'
      if (darkmodel .eq. 'Plummergen'
     &  .or. darkmodel .eq. 'PlummerGen'
     &  .or. darkmodel .eq. 'plummergen') then
         darkmodel = 'gPlummer'
      end if
      if (darkmodel .eq. 'kazantzidis'
     &   .or. darkmodel .eq. 'Kaz'
     &     .or. darkmodel .eq. 'kaz') then
         if (darkpar2 .eq. -1.d0) then
            darkmodel = 'Kazantzidis'
         else
            darkmodel = 'gKazantzidis'
         end if
      end if
      if (darkmodel .eq. 'Kazantzidisgen'
     &  .or. darkmodel .eq. 'KazantzidisGen'
     &  .or. darkmodel .eq. 'kazantzidisgen') then
         darkmodel = 'gKazantzidis'
      end if 
      if (darkmodel .eq. 'Cubic' .or. darkmodel .eq. 'cubic') then
         darkmodel = 'cNFW'
      end if
      if (darkmodel .eq. 'sersic' .or. darkmodel .eq. 'Sersic'
     &   .or. darkmodel .eq. 'PS' .or. darkmodel .eq. 'ps') then
         darkmodel = 'PrugnielSimien'
      end if
      if (darkmodel .eq. 'burkert') darkmodel = 'Burkert'
      if (darkmodel .eq. 'einasto') darkmodel = 'Einasto'
c
      do n = 1, ncomp
         if (tracermodel(n) .eq. 'nfw') tracermodel(n) = 'NFW'
         if (tracermodel(n) .eq. 'NFWgen'
     &        .or. tracermodel(n) .eq. 'nfwgen') then
            tracermodel(n) = 'gNFW'
         end if
         if (tracermodel(n) .eq. 'hernquist') then
            tracermodel(n) = 'Hernquist'
         end if      
         if (tracermodel(n) .eq. 'Hernquistgen'
     &        .or. tracermodel(n) .eq. 'HernquistGen'
     &        .or. tracermodel(n) .eq. 'hernquistgen') then
            tracermodel(n) = 'gHernquist'
         end if
         if (tracermodel(n) .eq. 'plummer') tracermodel(n) = 'Plummer'
         if (tracermodel(n) .eq. 'Plummergen'
     &        .or. tracermodel(n) .eq. 'PlummerGen'
     &        .or. tracermodel(n) .eq. 'plummergen') then
            tracermodel(n) = 'gPlummer'
         end if      
         if (tracermodel(n) .eq. 'kazantzidis'
     &        .or. tracermodel(n) .eq. 'Kaz'
     &        .or. tracermodel(n) .eq. 'kaz') then
            tracermodel(n) = 'Kazantzidis'
         end if
         if (tracermodel(n) .eq. 'Kazantzidisgen'
     &        .or. tracermodel(n) .eq. 'KazantzidisGen'
     &        .or. tracermodel(n) .eq. 'kazantzidisgen'
     &        .or. tracermodel(n) .eq. 'KazGen'
     &        .or. tracermodel(n) .eq. 'kazgen') then
            tracermodel(n) = 'gKazantzidis'
         end if 
         if (tracermodel(n) .eq. 'Cubic'
     &        .or. tracermodel(n) .eq. 'cubic') then
            tracermodel(n) = 'cNFW'
         end if
         if (tracermodel(n) .eq. 'sersic'
     &        .or. tracermodel(n) .eq. 'Sersic'
     &        .or. tracermodel(n) .eq. 'PS'
     &        .or. tracermodel(n) .eq. 'ps') then
            tracermodel(n) = 'PrugnielSimien'
         end if
         if (tracermodel(n) .eq. 'PrugnielSimienin') then
            tracermodel(n) = 'PSinf'
         endif
         if (tracermodel(n) .eq. 'jaffe') tracermodel(n) = 'Jaffe'
         if (tracermodel(n) .eq. 'king' .or. tracermodel(n) .eq. 'King'
     &        .or. tracermodel(n) .eq. 'modHubble') then
            tracermodel(n) = 'mHubble'
         end if      
c
         if (anismodel(n) .eq. 'iso') anismodel(n) = 'isotropic'
         if (anismodel(n) .eq. 'cst') anismodel(n) = 'constant'
         if (anismodel(n) .eq. 'T') anismodel(n) = 'Tiret'
         if (anismodel(n) .eq. 'gTiret') anismodel(n) = 'Tiret'
         if (anismodel(n) .eq. 'ML') anismodel(n) = 'MamonLokas'
         if (anismodel(n) .eq. 'OM') anismodel(n) = 'OsipkovMerritt'
         if (anismodel(n) .eq. 'OMgen' .or. anismodel(n) .eq. 'gOM')
     &      anismodel(n) = 'gOsipkovMerritt'
      end do
c
      if (v3dmodel .eq. 'gauss' .or. v3dmodel .eq. 'Gauss'
     &     .or. v3dmodel .eq. 'gaussian') v3dmodel = 'Gaussian'
c
c Constants
c
      one = 1.d0
      minusone = -1.d0
      pi = dacos(-1.d0)
      twopi = pi+pi
      sqrttwopi = dsqrt(twopi)
      third = 1.d0/3.d0
      gam3 = darkpar2+3.d0
      ! following 2 lines are only used for darkmodel = NFWgen
c$$$      rminoveradark = (1.d0+SMALL)*10.d0**(-3.0d0)
c$$$      rmaxoveradark = (1.d0-SMALL)*10.d0**2.6d0 / 3.d0
      rmaxoveradark = 1300.d0
c
c save normalization of gNFW mass profile
c
      if (darkmodel .eq. 'gNFW') then
         hya = gam3
         hyb = gam3
         hyc = gam3+1.d0
         xarg = -darkpar2-2.d0
         call HYGFX(hya,hyb,hyc,xarg,hypergeom1)
      endif
c
c if weight vector has NaNs or negatives, convert to 0s
c
      sumnonzero = 0
      do n = 1, ndata
         if (isnan(wt(n)) .or. wt(n) .lt. 0.d0) then
            wt(n) = 0.d0
         endif
         if (wt(n) .gt. 0.d0) then
            sumnonzero = sumnonzero + 1
         endif 
      enddo
c
c if all weights are 0, reset all to 1
c      
      if (sumnonzero .eq. 0) then
         do n = 1, ndata
            wt(n) = 1.d0
         enddo
      endif
c
      evz0 = 0.d0  
c
c H(z) for flat Universe
c
      Hofz = h*H100*Eofz(Omegam,1.d0-Omegam,z)
c
c interloper factor
c
c      ilopfactor = 10.d0**lBilop*dsqrt(2.d0/Delta)/Hofz
c
c     1st loop over tracer components:
c       set allowed limits and fiducial radii of each tracer component
c
      sumfractracer = 0.d0
      do n = 1, ncomp
c     
c        allowed limits set to data if < 0 (R_min) or <= 0 (R_max |v_z,max})
c     
         if (debug .ge. 2) then
            print *,' n rrmindata rrmaxdata avzmaxdata = ', n,
     &       sngl(rrmindata(n)), sngl(rrmaxdata(n)), sngl(avzmaxdata(n))
         endif
         if (rrminallow(n) .lt. 0.d0) then
            rrminallow(n) = rrmindata(n)
         endif
         if (rrmaxallow(n) .le. 0.d0) then
            rrmaxallow(n) = rrmaxdata(n)
         endif
         if (avzmaxallow(n) .le. 0.d0) then
            avzmaxallow(n) = avzmaxdata(n)
         endif
c     
         if (debug .ge. 1) then
            print *,' after setting allowed values ...'
            print *,' rrminallow rrmaxallow avzmaxallow = ',
     &        sngl(rrminallow(n)), sngl(rrmaxallow(n)), 
     &        sngl(avzmaxallow(n))
         endif
c     
c     change r_fidtracer < 0 to 10^tracerradius, r_fidtracer = 0 to HUGE (i.e. infinity)
c
         trmod = tracermodel(n)
         if (dabs(rfidtracer(n)) .lt. SMALL) then
            if (trmod .eq. 'NFW' .or. trmod .eq. 'cNFW'
     &        .or. trmod .eq. 'NFWgenout'
     &        .or. trmod .eq. 'nfwgenout') then
                 print *,' rfid cannot be 0 for NFW-like tracer ',
     &             '(diverges outwards)'
                 stop
            endif
            rfidtracer(n) = HUGE
         else if (rfidtracer(n) .lt. 0.d0) then ! 
            rfidtracer(n) = 10.d0**ltracerradius(n)
         endif
c
c        set last tracer fraction to 1 minus sum of previous ones (unless all 0)
c
         if (n .lt. ncomp) then
            sumfractracer = sumfractracer + fractracer(n)
         else if (sumfractracer .gt. SMALL) then
            fractracer(n) = 1.d0 - sumfractracer
         endif 
c
c        stop if fractracer is < 0 or > 1.
c
         if (fractracer(n) .lt. -SMALL .or. 
     &    fractracer(n) .gt. 1.d0+SMALL) then
            print *,' component n = ', n, ' fractracer = ', 
     &        fractracer(n)
            print *,' must be between 0 and 1!'
            stop
         endif
c
c        convert fractracer to tracermass if necessary
c
         if (fractracer(n) .gt. 0.d0) then
            ltracermass(n) = ltracermasstot + dlog10(fractracer(n))
         else if (dabs(fractracer(n)) .lt. SMALL
     &     .and. ltracermass(n) .lt. -1.d0) then
            ltracermass(n) = -99.d0
         endif
c
      end do  ! end of 1st loop over tracer components
c
c TLM (tied-light-mass): force a_tracer = a_dark for all tracer components
c
      if (TLMflag .ne. 1) then
         do n = 1, ncomp
            atracerrad(n) = 10.d0**ltracerradius(n)
         enddo   
      endif
c
c copy variables for MODEL common block (model2.i)
c
      distflagg = distflag
      darkmod = darkmodel
      darkparam2 = darkpar2
      v3dmod = v3dmodel
      rmaxx = rmax
      if (debug .ge. 2) then
         print *,' 0) rmaxx = ', rmaxx
      endif
      mu00 = mu0
      aa0lclM = a0lclM
      aa1lclM = a1lclM
      fourpideltarhocover3 = Delta/2.d0*Hofz*Hofz/GNewton ! 4pi/3 Delta rho_crit = M_vir/r_vir^3
      MBH11 = 10.d0**(lbhmass-11.d0)    ! black hole mass in 10^11 M_Sun
c
c dark matter scale or concentration
c
c      print *,' before 1st block: darkscale = ', darkscale
      if (MfLflag .eq. 1) then          ! dark scale = scale of 1st tracer component
         adark = 10.d0**ltracerradius(1)
         if (debug .ge. 3) then
            print *,' adark = atracer(1) = ', sngl(adark)
         endif
      else if (darkscaleflag .eq. 1) then
         adark = 10.d0**darkscale
         if (darkmodel .eq. 'gNFW') then
            rmaxx = adark*rmaxoveradark
         endif   
         if (debug .ge. 3) then
            print *,' adark = 10^darkscale = ', sngl(adark)
         endif
      else if (darkscaleflag .eq. 2) then
         c = 10.d0**darkscale
      else
         print *,' MAMPOSSTLNLIK: cannot recognize darkscaleflag = ',
     &    darkscaleflag
         stop
      endif
c      print *,' past 1st block: atracer adark darkscaleflag = ', 
c     &  atracer, adark, darkscaleflag
c
c dark matter normalization
c
c 1) know r_vir,dark: M_v,d = Delta/2 H(z)^2 r_v,d^3/G
c
c 2) know M_vir,dark: r_v,d = (2 G M_v,d/[Delta H(z)^2])^(1/3)
c
c 3) know M_dark(r_fid): 
c
c then have equation: M_d(r_fid) Mtilde_d(c)/Mtilde_d(r_fid/a_d) = Delta/2 H(z)^2 a_d^3 c^3/G
c
c 3a) know c: solve equation for a_d
c
c 3b) know a_d: solve equation for c
c
c in both cases:
c    r_v,d = c a_d and M_v,d = Delta/2 H(z)^2 r_v,d^3/G
c    need to check that M_v,d >= 0 if it is total M_v computed
c
      if (dabs(darknormflag-(-1.d0)) .lt. SMALL) then ! Case 1: r_vir
         rvirdark = 10.d0**norm
         Mvirdark11 = fourpideltarhocover3*rvirdark**3.d0
         rdark = rvirdark
         Mdark11 = Mvirdark11
c
c convert from total to dark
c and exit if dark mass is negative at r = r_v^dark
c
      else if (dabs(darknormflag) .le. SMALL) then ! Case 2: M_vir
         Mvirdark11 = 10.d0**norm/1.d11
         rvirdark = (Mvirdark11/fourpideltarhocover3)**third
         rdark = rvirdark
         Mdark11 = Mvirdark11
c
      else if (dabs(darknormflag) .gt. SMALL) then ! Case 3: M(r_fid)
         rfiddark = darknormflag
         rdark = rfiddark
         Mfiddark11 = 10.d0**norm/1.d11
         Mdark11 = Mfiddark11
      else
         print *,' MAMPOSSTLNLIK: cannot recognize darknormflag = ',
     &        darknormflag
         stop
      endif ! end of dark IF block
c
      if (debug .ge. 2) then
         print *,' past 2nd block: adark = ', sngl(adark), 
     &   ' Mdark11 = ', sngl(Mdark11)
      endif
c
c If darktotflag = 2 (total mass computation):
c   check that dark component has non-negative virial mass
c   at r_dark (scale radius or fiducial radius)
c
      if (darktotflag .eq. 2) then
         ! tracer mass at r_dark
         Mtracer11 = 0.d0
         do n = 1, ncomp
            par2 = tracerpar2(n)
            Mtracer11 = Mtracer11 + 10.d0**ltracermass(n)/1.d11
     &       *mass(rdark/atracerrad(n),tracermodel(n),par2)
     &       /mass(rfidtracer(n)/atracerrad(n),tracermodel(n),par2)
         enddo
c
         Mdark11 = Mdark11 - Mtracer11 - MBH11
         if (Mdark11 .lt. -SMALL) then
            print *,' M_dark at r=',sngl(rdark), ' < 0'
            mamposstlnlik = HUGE
            return
         else if (darknormflag .gt. SMALL) then ! M(r_fid)
            Mfiddark11 = Mdark11
         else
            Mvirdark11 = Mdark11
            rvirdark = (Mvirdark11/fourpideltarhocover3)**third
         endif
      endif
c
c Compute virial parameters when r_fid^dark (i.e. darknormflag) is given (Cases 3a and 3b)
c
      if (darknormflag .gt. SMALL) then
      ! MfLflag is forbidden above
         
         if  (a0lclM .gt. 0.d0 .or. darkscaleflag .eq. 2) then
         ! Case 3a: M(r_fid) where c is known (or from c-M): solve for log a_dark
            par2 = darkpar2
            if (a0lclM .gt. 0.d0) then
               Mfid11 = Mfiddark11
            else
               rcube = mass(c,darkmodel,par2)/(c*c*c)
     &          /fourpideltarhocover3*Mfiddark11
            endif 
            ldarkradiusmin = -1.d0
            ldarkradiusmax = 3.d0
            ipass = 0
            ier = 65
            do while (ipass .lt. 3 .and. ier .gt. 64)
               ipass = ipass + 1
               tol = 1.d-4
               ldarkradius = zero(ldarkradiusmin,ldarkradiusmax,
     &          tol,fldarkradius,ier)
               if (ier .eq. 1) then
                  print *,' ZERO: fldarkradius has = sign at extremes'
                  print *,' c rcube Mfid11 ',
     &             sngl(c), sngl(rcube), sngl(Mfiddark11)
                  print *,' rfiddark darkpar2 = ', sngl(rfiddark), 
     &             sngl(darkpar2)
                  print *,' i lg(adark) fldarkradius(log10(adark))'
                  if (rcube .le. SMALL) then
                     print *,' M~(c) = ', sngl(mass(c,darkmodel,par2))
                     print *,' M~(r_v)/r_v^3 = ',
     &                 sngl(fourpideltarhocover3)
                     stop
                  endif
                  found = 0
                  do i = 1, 11
                     ldarkradius = ldarkradiusmin + 
     &                 0.1d0*(ldarkradiusmax-ldarkradiusmin)*dfloat(i-1)
                     fld = fldarkradius(ldarkradius)
                     write(*,9129) i, ldarkradius, fld
 9129                format(i3,f10.3,0pe13.5)
                     if (i .eq. 1) then
                        sgn1 = sgn(fld)
                     else if (sgn(fld) .ne. sgn1) then
                        ldarkradiusmax = ldarkradius
                        found = 1
                        exit
                     endif   
                  enddo 
                  if (found .eq. 0) then
                     ldarkradiusmin = ldarkradiusmin - 1.d0
                     ldarkradiusmax = ldarkradiusmax + 1.d0
                  endif      
                  if (ipass .lt. 3) then
                     print *,' reiterating with ldarkradius between ',
     &                  sngl(ldarkradiusmin), ' and ' , 
     &                  sngl(ldarkradiusmax)
                  endif
               endif
            enddo
            if (ipass .gt. 1) then
               print *,' out of ZERO loop : ipass = ', ipass, 
     &           ' ldarkradiusmax = ', sngl(ldarkradiusmax)
            endif   
            adark = 10.d0**ldarkradius
            if (debug .ge. 2) then
               print *,' solved and found adark = ', sngl(adark)
            endif
         else if (darkscaleflag .eq. 1) then
         ! Case 3b: M(r_fid) where a_dark is known:  solve for log c
            par2 = darkpar2
            rhs = fourpideltarhocover3*adark**3.d0
     &           *mass(darknormflag/adark,darkmodel,par2)/Mfiddark11
            lcmin = -0.5d0
            lcmax = 3.d0
            ipass = 0
            ier = 1
            do while (ipass .lt. 3 .and. ier .gt. 0)
               ipass = ipass + 1
               tol = 1.d-4
               lc = zero(lcmin,lcmax,tol,flc,ier)
               if (debug .ge. 2) then
                  print *,' past ZERO on c, ier = ', ier
               endif 
               if (ier .eq. 1) then
                  print *,' ZERO: flc has same sign at extremes'
                  print *,' adark rfiddark Mfiddark11 rhs =',
     &               sngl(adark), sngl(darknormflag), sngl(Mfiddark11),
     &               sngl(rhs)
                  print *,' i c rvird Mvird11 Mvird11bis f'
                  do i = 1, 11
                     lc = lcmin + 
     &                    0.1d0*(lcmax-lcmin)*dfloat(i-1)
                     par2 = darkpar2
                     if (debug .ge. 2) then
                        print *,' before write in ZERO loop'
                     endif   
                     write(*,9130) i, 10.d0**lc, 10.d0**lc*adark, 
     &                 fourpideltarhocover3*(10.d0**lc*adark)**3.d0,
     &                 Mfiddark11/
     &                 mass(darknormflag/adark,darkmodel,par2)*
     &                 mass(10.d0**lc,darkmodel,par2), flc(lc)
 9130                format(i3, 4(f13.3), e13.5)
                  enddo 
c
c   Widen interval to find solution
c
                  lcmin = lcmin - 1.d0
                  lcmax = lcmax + 1.d0
c$$$                  if (darkmodel .eq. 'gNFW') then
c$$$                     lcmax = dmin1(lcmax,rmaxoveradark)
c$$$                  endif  
                  if (ipass .lt. 3) then
                     print *,' reiterating ...'
                  endif
               endif    ! end of ZERO ier diagnstics
            enddo
            if (ipass .gt. 1) then
               print *,' out of ZERO loop : ipass = ', ipass, 
     &           ' lcmax = ', lcmax
            endif   
            c = 10.d0**lc
         endif      ! end of darkscaleflag=2 & ! MfLflag or TLMflag
         rvirdark = c*adark
         Mvirdark11 = fourpideltarhocover3*rvirdark**3.d0
      else if (darknormflag .le. SMALL) then ! darknormflag = -1 (r_vir) or 0 (M_vir)
c
c set c or a_dark knowing r_vir,d
c
c$$$         if (a0lclM .gt. 0.d0 .and. dabs(darknormflag) .le. SMALL) then ! known c-M relation
c$$$            c = 10.d0**(a0lclM + a1lclM*norm)
c$$$            adark = rvirdark/c
c$$$  else
         if (darkscaleflag .eq. 2) then
            c = 10.d0**darkscale
            if (MfLflag .ne. 1) then
               adark = rvirdark/c
               if (debug .ge. 3) then
                  print *,' darknormflag = ', sngl(darknormflag),
     &              ' < SMALL=', SMALL
                  print *,' rvirdark = ', sngl(rvirdark), ' c=',sngl(c)
                  print *,' new adark = ', sngl(adark)
               endif
            endif
         else
            c = rvirdark/adark
            if (debug .ge. 3) then
               print *,' found c = ', c
            endif
         endif
      endif     ! end of darknormflag IF block
c
      if (debug .ge. 1) then
         print *,' rvir adark c = ', sngl(rvir), sngl(adark), sngl(c)
c         print *,' rvirdark Mvirdark11 = ', rvirdark, Mvirdark11
      endif
c
c virial radius for total mass
c
c 1) log M_d - log M_tracer > 6: r_v = r_v,d, M_v = M_v,d
c
c 2) log M_d - log M_tracer < 6:
c
c solve for r_v:
c 
c M_d,v Mtilde(r_v/a_d)/Mtilde(c) + sum_comp M_tr(a_tr) Mtilde_tr(r_v/a_tr) + M_BH 
c       = Delta/2 H(z)^2 r_v^3/G 
c
      par2 = darkpar2
      Mdarkatad11 = Mvirdark11/mass(c,darkmodel,par2)    ! THIS DEPENDS ON DARKTOTFLAG!
c  sum of tracer masses at rvirdark
      Mtraceratrvdark11 = 0.d0
c save tracer component paramters to COMPONENTS common block
      ncomps = ncomp
      do n = 1, ncomp
         par2 = tracerpar2(n)
         if (debug .ge. 2) then
            print *,' comp n = ', n
            print *,' tracermodel = ', tracermodel(n)
            print *,' tracerpar2 = ', tracerpar2(n)
            print *,' a_tr = ', atracerrad(n)
         endif
         atr = atracerrad(n)
         tracmod(n) = tracermodel(n)
         lMtr(n) = ltracermass(n)
         atrac(n) = 10.d0**ltracerradius(n)
         rfidtr(n) = rfidtracer(n)
         tracerp2(n) = tracerpar2(n)
         Mtr11 = 10.d0**ltracermass(n)/1.d11
c$$$         Mtratatr11(n) = Mtr11
c$$$     &        /mass(rfidtracer(n)/atracerrad(n),tracermodel(n),par2)
         Mtraceratrvdark11 = Mtraceratrvdark11 + Mtr11
     &        *mass(rvirdark/atr,tracermodel(n),par2)
     &        /mass(rfidtracer(n)/atr,tracermodel(n),par2)
      enddo                     ! end of loop small on components
c$$$  Mvirdark11 = Mvirdark11 - MBH11 - Mtraceratrvdark11
      if (debug .ge. 3) then
         print *,' M_tr11(a_tr)=',sngl(Mtratatr11)
         print *,' M_d11(a_d)=',Mdarkatad11
         print *,' M_d11(r_vir)=',Mvirdark11
         print *,' rvirdark = ', rvirdark
      endif
c
      lMvirdark11 = dlog10(Mvirdark11)
c
c mean log concentration
c
      if (a0lclM .gt. 0.d0) then
         lcmean = a0lclM + a1lclM*(11.d0+lMvirdark11)
      endif
      lMtraceratrvird11 = dlog10(Mtraceratrvdark11)
      if (lMvirdark11-lMtraceratrvird11 .gt. 6.d0) then
         rvir = rvirdark
         Mvir11 = Mvirdark11
         if (debug .ge. 3) then
            print *,' rvir = ', sngl(rvir)
         endif
      else if (MfLflag .eq. 0 .and. TLMflag .eq. 0) then 
! solve equation for r_vir^total
         lrvirmin = dlog10(rvirdark) - 2.d0
         lrvirmax = dlog10(rvirdark) + 2.d0
! make sure we are within fit to M_gNFW(r)
c$$$         if (darkmodel .eq. 'gNFW') then
c$$$            lrvirmin = dmax1(lrvirmin,dlog10(adark*rminoveradark))
c$$$            lrvirmax = dmin1(lrvirmax,dlog10(3.d0*adark*rmaxoveradark))
c$$$         endif
         if (debug .ge. 2) then
            print *,' before rvir solution lrvirmin lrvirmax = ', 
     &           lrvirmin, lrvirmax
         endif
         ipass = 0
c$$$         ier = 65
c$$$         do while (ipass .lt. 3 .and. ier .gt. 64)
         ier = 1
         do while (ipass .lt. 3 .and. ier .gt. 0)
            ipass = ipass + 1
            tol = 1.d-4
            lrvir = zero(lrvirmin,lrvirmax,tol,flrvir,ier)
            if (ier .eq. 1) then
               print *,' ZERO: flrvir has same sign at extremes'
               print *,' Mdarkatad11 adark =', Mdarkatad11, adark
               print *,' i lg(rvir) flrv(lg(rvir)) [lrvir]'
               do i = 1, 11
                  lrvir = lrvir +
     &              0.1d0*(lrvirmax-lrvirmin)*dfloat(i-1)
                  write (*,9129) i, lrvir, flrvir(lrvir)
               enddo 

! check for negative signs at min and max tries for r_vir

               if (flrvir(lrvirmin) .lt. 0.d0 .and. 
     &              flrvir(lrvirmax) .lt. 0.d0) then
! check for positive sign in between
                  do i = 11, 1, -1
                     lrvir = lrvirmin + 
     &                 0.1d0*(lrvirmax-lrvirmin)*dfloat(i-1)
                     if (flrvir(lrvir) .gt. 0.d0) then
                        lrvirmax = lrvir
                        print *,' reiterating with lower lrvirmax = ',
     &                   lrvirmax
                        exit
                     endif
                  enddo 
               else
c
c widen interval for minimum virial radius
c
                  lrvirmin = lrvirmin - 1.d0
                  print *,' reiterating with lower lrvirmin = ', 
     &              lrvirmin
               endif  ! end of if (flrvir > 0 at both extremities)
            endif ! end of if (ier=130)
         enddo ! end of do while
         if (ipass .gt. 1) then
            print *,' out of ZERO loop : ipass = ', ipass, 
     &       ' lrvirmax = ', lrvirmax
         endif   
         rvir = 10.d0**lrvir
         Mvir11 = fourpideltarhocover3*rvir**3.d0
         if (debug .ge. 2) then
            print *,' rvir = ', sngl(rvir)
         endif
      else
         ok = 1
         if (MfLflag .eq. 1) then
            print *,
     &       ' MAMPOSSTLNLIK: cannot have MfLflag=1 & ',
     &       ' log(M_dark^vir/M_tracer) < 6'
            print *,' log M_tr(r_v,d) log M_d(r_v,d) = ', 
     &       lMtraceratrvird11, lMvirdark11
c            print *,' Mtraceratatr11 = ', Mtraceratatr11
            ok = 0
         endif
         if (TLMflag .eq. 1) then
            print *,
     &       ' MAMPOSSTLNLIK: cannot have TLMflag=1 & ',
     &       ' log(M_dark^vir/M_tracer) < 6'
            ok = 0
         endif
         if (ok .eq. 0) then
            stop
         endif
      endif
c
      if (TLMflag .eq. 1) then  ! Tied-Light-Mass
         do n = 1, ncomp
            atracerrad(n) = adark
         enddo
      endif
c
c rmax must be > 10 r_vir
c
c$$$      if (rmax .lt. 10.d0*rvir .and. darkmodel .ne. 'gNFW') then
      if (rmax .lt. 10.d0*rvir) then
         rmax = 10.d0*rvir
         rmaxx = rmax
         if (debug .ge. 2) then
            print *,' 1) rmaxx = ', rmaxx
         endif
      endif
c
c cubic spline dimensions
c
c
c g normalization (e.g. for TLM)
c DeltaNproj = Delta M~_p
c
c 2nd (big) loop over tracer components
c
      if (debug .ge. 1) then
         print *,' nd comp = ', (nd,comp(nd),nd=1,ndata,100)
      end if
      mamposstlnlik = 0.d0
      do n = 1, ncomp
c
c        skip components with no data
c
         if (rrmindata(n) .lt. -SMALL
     &    .or. rrmaxdata(n) .lt. -SMALL) then
            cycle
         endif
c
c component variables for MODEL common block
c
         if (debug .ge. 1) then
            print *,' component n = ', n, ' ...'
         endif
         tracermod = tracermodel(n)
         if (debug .ge. 2) then
            print *,' tracermod = ', tracermod
         endif
         anismod = anismodel(n)
         atracer = 10.d0**ltracerradius(n)
         tracerparam2 = tracerpar2(n)
c
c convert anisotropies to beta values (if necessary) and save in common block
c
         if (anisflag .eq. 0) then  ! log (sigma_r / sigma_theta)
            bet0 = 1.d0 - 10.d0**(-2.d0*lanis0(n))
            betinf = 1.d0 - 10.d0**(-2.d0*lanisinf(n))
         else if (anisflag .eq. 1) then ! beta
            bet0 = lanis0(n)
            betinf = lanisinf(n)
         else if (anisflag .eq. 2) then ! beta_sym = beta/(2-beta) = (s_r^2-s_th^2)/(s_r^2+s_th^2)
            bet0 = 2.d0 * lanis0(n) / (1.d0+lanis0(n))
            betinf = 2.d0 * lanisinf(n) / (1.d0+lanisinf(n))
         else if (anisflag .eq. 22) then ! beta_sym = beta/(1-beta/2) = 2 (s_r^2-s_th^2)/(s_r^2+s_th^2)
            bet0 = lanis0(n) / (1.d0+0.5d0*lanis0(n))
            betinf = lanisinf(n) / (1.d0+0.5d0*lanisinf(n))
         else
            print *,' MAMPOSSTLNLIK39: cannot recognize anisflag = ', 
     &       anisflag
            stop
         end if 
c
c TALflag=1: force anisotropy radius = tracer radius for all componenents
c
         if (TALflag .eq. 1) then
            anisrad = atracer
         else
            anisrad = 10.d0**lanisradius(n)
         endif
         anismod = anismodel(n)
c
c shortcuts
c
         atr = atracer
         trmodel = tracermodel(n)
         ctr = rvir/atr
         xxmin = rrminallow(n)/rvir
         xxmax = rrmaxallow(n)/rvir
c
c     pre-compute sigma_r(r) in bins of ln r:
c     
c     1) prepare ranges for ln radius & ln projected radius
c     
C$$$  lnrrmindata = dlog(rrmindata(n))
C$$$  lnrrmaxdata = dlog(rrmaxdata(n))
         lnrmax = dlog(rmax)
         ! extend to 3*rmax to be able to perform outward integral for large r
         lnrmax3 = dlog(3.d0*rmax)
         ! make sure we are within fit to M_gNFW(r)
         ! multiply by adark*(gamma+2) = r_{-2}
c$$$         if (darkmodel .eq. 'gNFW') then
c$$$            lnrmax3 = dmin1(lnrmax3,dlog(3.d0*adark*rmaxoveradark))
c$$$            lnrmax = lnrmax3 - dlog(3.d0)
c$$$         endif
c     
c     2) prepare loop over radii
c     
         nrbins = 21
         if (nrbins .gt. MAXRBINS) then
            print *,' nrbins must be <= ', MAXRBINS
            stop
         endif
         if (rrminallow(n) .eq. 0.d0) then
            lnrrmin = -7.d0
c$$$            if ((darkmodel .eq. 'gNFW')
c$$$     &       .and. lnrrmin .lt. dlog(rminoveradark*adark)) then
c$$$               lnrrmin = dlog(rminoveradark*adark)
c$$$            endif    
         else
            lnrrmin = dlog(rrminallow(n))
         endif
         if (debug .ge. 2) then
            print *,' n rrminallow(n) lrrmin = ', n,
     &       rrminallow(n), lnrrmin
         endif
         dlnr = (lnrmax-lnrrmin)/dfloat(nrbins-1)*(1.d0 + SMALL)
         dlnr0 = (lnrmax-lnrrmin)/dfloat(nrbins-1)
! note: ln r_max - ln R_min
c$$$         aerr = 0.d0
         rerr = 1.d-4
! sigma_r integral goes to 3 r_max to obtain q integral to r_max
! extra SMALL increment is to make sure that 
! no extrapolation is required for outermost data
         if (debug .ge. 2) then
            print *,' nrbins lnrrmin dlnr = ', nrbins, lnrrmin, dlnr
         endif
c
c     3) loop over radii
c
         do nrb = 1, nrbins
            if (debug .ge. 2) then
               print *,' nrb lnrmin dlnr = ', nrb,
     &              lnrrmin, dlnr
            endif
            lnr(nrb) = lnrrmin + dfloat(nrb-1)*dlnr
            if (lnr(nrb) .gt. lnrmax) then
               exit
            endif
            rad = dexp(lnr(nrb))
            if (debug .ge. 2) then
               print *,' before integral of FNUSIGR2:'
               print *,' nrb lnrmin = ', nrb, lnr(nrb)
               print *,' lnrmax3 =', lnrmax3
            endif
c     
c     4) evaluate sigma_r(r) by integrating Jeans equation
c     
            limit = MAXSPLITINTEG
            lenw = 4*limit
! next two lines have been assigned outside of the loop, but kept for safety
! because aerr can be set to tiny value on output of dqags
            
c$$$            aerr = 0.d0
            rerr = 1.d-4        ! must be reset for DGAUS8
c$$$            call dqags(fnusigr2,lnr(nrb),lnrmax3,aerr,rerr,sum,aerror,
c$$$     &           neval,ier,limit,lenw,last,iwork,work)
            call dgaus8(fnusigr2,lnr(nrb),lnrmax3,rerr,sum,ier)
            if (ier. ne. 1) then
               print *,' integral of FNUSIGR2 (Jeans) has problems ',
     &              ' for component ', n,
     &              ' from r = ',  sngl(dexp(lnr(nrb))),
     &              ' to r = ', sngl(dexp(lnrmax3))
               call gaus8err(ier)
               print *,' aerr = ', aerr, ' rerr = ', rerr
               print *,' limit = ', limit, ' lenw = ', lenw,
     &          ' last = ', last
               print *,' absolute error = ', aerror, ' Neval= ', neval
               print *,' num subintervals = ', last
               print *,' beta0 = ', bet0, ' betinf = ', betinf, 
     &              ' anisr = ',  anisrad
               print *,' adark = ', adark, ' norm = ', norm
               print *,' atr = ', atr
               debugg = 4
               do nrbdum = nrb, nrbins
                  lnrdum = lnr(nrbdum)
                  ! print *,' s = ', dexp(lnrdum)
                  fnusgr2 = fnusigr2(lnrdum)
                  ! print *,' fnusigr2 = ', fnusgr2
               enddo
               stop
            end if
            if (isnan(sum)) then
               print *,' sum (fnusigr2) = NAN'
               print *,' ln r_min = ', lnr(nrb), ' lr r_max = ', lnrmax3
            endif
            par2 = tracerpar2(n)
            dens = density(rad/atr,trmodel,par2)
c     
c           only save for reasonable density and integral
c     
            if (sum .lt. TINY .or. dens .lt. TINY 
     &           .or. isnan(sum) .or. isnan(dens)) then
               if (debug .ge. 2) then
                  print *,' nrb = ', nrb, ' r = ', rad
                  if (sum .lt. TINY) then
                     print *,' sum = ', sum, ' < TINY=', TINY
                  endif
                  if (dens .lt. TINY) then
                     print *,' dens = ', dens, ' < TINY=', TINY
                  endif
                  if (isnan(sum)) then
                     print *,' sum = ', sum
                  endif
                  if (isnan(dens)) then
                     print *,' dens = ', dens
                  endif
               endif

               exit
            endif

            sigmar = 100.d0*dsqrt(GNewton*sum/dens) ! in km/s
            lsigmar(nrb) = dlog10(sigmar)
            if (debug .ge. 1 .and. nrb .ge. 10 .and. nrb .le. 12) then
                  print *,' rad sigmar = ', rad, sigmar
            endif
c     
c       5) prepare input to interpolation routine for sigma_r(r): 
c       copy variables for common blocks
c     
            lnr2(nrb) = lnr(nrb)
            lsigmar2(nrb) = lsigmar(nrb)
c     
c           negative potential (which is positive) in (km/s)^2
c     
         enddo  ! end of loop over radial bins
c     
         nrbinsold = nrbins
         nrbins = nrb - 1
c     
c        if too few radial bins are OK return -ln L = HUGE
c
         if (nrbins .le. 2) then
            print *,' only ', nrbins, 
     &      ' radial bins with realistic sigma_r'
            print *,' assume -lnL = HUGE'
            print *,' '
            mamposstlnlik = HUGE
            return
         endif
c
c        reset r_max to stop at radius where density is TINY
c
         if (nrbins .lt. nrbinsold) then
            rmaxx = dmin1(rmaxx,dexp(lnr2(nrbins))*(1.d0-SMALL))
            if (debug .ge. 2) then
               print *,' 2) rmaxx = ', rmaxx
               print *,' nrbins = ', nrbins
               print *,' lnrrmin = ', sngl(lnrrmin)
               print *,' dlnr = ', sngl(dlnr)
            endif    
         endif
c     
c     6) save to spline common block
c     
         nrbins2 = nrbins
c     
c     7) cubic-spline coefficients for ln sigma_r vs ln r and ln r vs -Phi
c     
c$$$         gamma = 0.d0
c$$$         call TAUTSP(lnr2,lsigmar2,nrbins2,gamma,wk,breaksigr2,
c$$$     &      csigr2,icsigr2,kdimsigr2,ier)
         call SPLINE_CUBIC_SET(nrbins2,lnr2,lsigmar2,2,0.d0,2,0.d0,ypp2)
         lnr1(1) = dlog(rvir)
c    
C$$$  call icsccu(phi2,lnr2,nrbins2,cphi2,icsigr2,ier)
c     
c     8) save spline coefficients to common block
c     
         do nrb = 1, nrbins
            ypp(nrb) = ypp2(nrb)
         end do

c     end of sigma_r(r) computation
c
         p2tr = tracerpar2(n)
c
c     change r_max to r_vir if interlopers included 
c     so that g integral is evaluated properly for halo term
c     
         if (mod(ilopflag,2) .eq. 0) then
            rmaxx = rvir
            if (debug .ge. 2) then
               print *,' 3) rmaxx = ', rmaxx
            endif
         endif
c     
         vvir = 100.d0*dsqrt(Delta/2.d0)*Hofz*rvir ! in km/s
         kappa = avzmaxallow(n)/vvir
         Bilop = 10.d0**lBilop
c
c denominators of halo and interlopers, when possible (no split)
c
         denomilop = 0.d0
         denom0ilop = 0.d0
         if (splitpflag .eq. 0) then
            if (ilopflag .eq. 1) then
               denomhalo = massproj(rrmaxallow(n)/atr,trmodel,p2tr)
     &          - massproj(rrminallow(n)/atr,trmodel,p2tr)
               denomilop = twopi*mass(ctr,trmodel,p2tr)
     &          *kappa*Bilop*(xxmax*xxmax-xxmin*xxmin)
            else
               denomhalo =
     &          massprojsph(rrmaxallow(n)/atr,ctr,trmodel,p2tr)
     &          - massprojsph(rrminallow(n)/atr,ctr,trmodel,p2tr)
               if (ilopflag .eq. 2) then
                  denomilop = twopi*mass(ctr,trmodel,p2tr)
     &             *(kappa*Bilop*(xxmax*xxmax-xxmin*xxmin)
     &               + dsqrt(twopi)*(Iilopapx(xxmax)-Iilopapx(xxmin)))
               endif
            endif
         else if (ilopflag .eq. 1) then ! split case
            denomilop = twopi*mass(ctr,trmodel,p2tr)/ctr**2.d0
     &       *kappa*Bilop
         endif ! splitpflag=1 case depends on data (on R), except for ilop,ilopflag=0,1
         
         if (splitpflag .eq. 1 .and. sigltracerradius(n) .le. 0.d0) 
     &    then ! to solve for a_tr
            if (ilopflag .eq. 1) then
               denom0halo = massproj(rrmaxallow(n)/atr,trmodel,p2tr)
     &          - massproj(rrminallow(n)/atr,trmodel,p2tr)
               denom0ilop = twopi*mass(ctr,trmodel,p2tr)
     &          *kappa*Bilop*(xxmax*xxmax-xxmin*xxmin)
            else
               denom0halo = massprojsph(rrmaxallow(n)/atr,ctr,trmodel
     &          ,p2tr)
     &          - massprojsph(rrminallow(n)/atr,ctr,trmodel,p2tr)
               if (ilopflag .eq. 2) then
                  denom0ilop = twopi*mass(ctr,trmodel,p2tr)
     &             *(kappa*Bilop*(xxmax*xxmax-xxmin*xxmin)
     &               + dsqrt(twopi)*(Iilopapx(xxmax)-Iilopapx(xxmin)))
               endif
            endif
            denom0 = denom0halo + denom0ilop
         endif
         
         ilopflagg = ilopflag
         par2 = tracerparam2
         vir2code = twopi*dsqrt(2.d0/Delta)*mass(ctr,trmodel,par2)
     &    / (Hofz*ctr**3.d0)    ! conversion of N_v/(r_v^2 v_v) to N(a)/(2 pi a^3)
c
c     maximum physical radii from data
c     
         if (debug .ge. 2) then
            print *,'  DeltaNpa3 =', sngl(DeltaNprojtildea3)
            print *,' entering 1st big data loop for comp ', n
         endif
c     
c     loop over data
c     
         lnlik(n) = 0.d0
         do nd = 1, ndata
c
c shortcuts
c
            compon = comp(nd)
            if (debug .ge. 2) then
               print *,' nd compon = ', nd, compon
            endif
!     read compon before underscore
            compon2 = compon(1:index(compon,'_')-1)
            read(compon2,*) icomp
            if (icomp .ne. n) cycle
            if (debug .ge. 2) then
               print *, ' icomp = ', icomp
               print *,' rrminallow = ', rrminallow(n)
            endif
            rrn = rr(nd)
            avzn = avz(nd)
            evzn = evz(nd)
            mun = mu(nd)
            emun = emu(nd)
c
c only consider data in allowed limits
c
            if (rrn .lt. rrminallow(n) .or. rrn .gt. rrmaxallow(n) 
     &           .or. (avzmaxallow(n) .gt. 0.d0
     &                 .and. avzn .gt. avzmaxallow(n))) then
               if (debug .ge. 1) then
                  print *,' R = ', rrn
                  print *,' Rminallow = ', rrminallow(n)
                  print *,' Rmaxallow = ', rrmaxallow(n)
                  print *,' |v_z| = ', avzn
                  print *,' avzmaxallow = ', avzmaxallow(n)
                  print *,' rejecting n_data = ', nd
               end if        
               cycle
            end if
c     
c     maximum physical radius r_max
c     
c$$$  phimax(1) = -0.5d0*avzn*avzn
      ! interpolate ...
c
c numerators for halo and interloper terms
c
            if (mun .gt. 10.d0 .and. distflag .gt. 0) then ! use distance indicator
               par2 = tracerpar2(n)
               ghalofac = 2.d0*atr*surfdensity(rrn/atr,trmodel,par2)
            else
               ghalofac = rrn
            endif
            numhalo = ghalofac * g(rrn,avzn,evzn,mun,emun) ! in N(a) R /a^3
            if (ilopflag .eq. 0) then
               numilop = 0.d0
            else if (ilopflag .eq. 1) then
               numilop = vir2code*Bilop
            else if (ilopflag .eq. 2) then
               numilop = vir2code*gilophat(xx,avzn/vvir,Bilop)
            else
               print *,' MAMPOSSTLNLIK: cannot recognize ilopflag=',
     &          ilopflag
               stop
            endif
c
c R-dependent denominators for halo and interloper terms
c
            if (splitpflag .eq. 1) then
               denomhalo = surfdensity(rrn/atr,trmodel,par2)
               if (ilopflag .eq. 1) then
                    denomilop = twopi*mass(ctr,trmodel,par2)/ctr**2.d0
     &               *kappa*Bilop
               else if (ilopflag .eq. 2) then
                    Xvir = rrn/rvir
                    sigi = sigmailop(Xvir)
                    denomilop = twopi*mass(ctr,trmodel,par2)/ctr**2.d0
     &               *(kappa*Bilop 
     &                + dsqrt(0.5d0*pi)*Ailop(Xvir)*sigi
     &                  *erf(kappa/sigi/dsqrt(2.d0)))
               endif
            endif
c
c ln prob(R,v):
c
c Joint probability q(R,v) = R/a_tr^3 num/denom
c 
c Split case p(v|R) = 1/(2a_tr) num/denom
c
            num = numhalo + numilop
            denom = denomhalo + denomilop
            if (debug .ge. 1 .and. mod(nd,100) .eq. 1) then
               print *,' nd numhalo denomhalo = ', nd,numhalo,denomhalo
c$$$               print *,' numilop denomilop = ', numilop, denomilop
            endif
            if (num .le. 0.d0) then
               lnpfinal(nd) = -1.d0*HUGE
            else if (splitpflag .eq. 0) then ! ln q(R_i,v_i)
               lnpfinal(nd) = dlog(rrn/atr**3.d0*num/denom)
            else ! splitpflag = 1 -> ln p(v_i|R_i)
               lnpfinal(nd) = dlog(0.5d0/atr*num/denom)
            endif
c
            if (debug .ge. 2) then
               write(*,9901) nd,rrn,avzn,mun,emun,numhalo,numilop,
     &              denomhalo,denomilop,lnpfinal(nd),wt(nd)
 9901          format(i3,x,f5.0,x,f6.0,x,2(f5.2,x),6(1pg12.4,x))
            endif
c
c fit log tracer scale radius to distribution of projected radii
c
            if (splitpflag .eq. 1 .and. sigltracerradius(n) .le. 0.d0) 
     &        then
               num0 = denom
               lnp0(nd) = dlog(2.d0*rrn/atr**2.d0*num0/denom0)
            endif
c
c update ln likelihood (with weights)
c
c Joint case: -ln L = - sum_i w_i ln q(R_i,v_i)
c
c Split-external case: -ln L = - sum_i ln p(v_i|R_i) + (log a_tr-log a_tr^ext)^2 / 2 (sig-log a_tr)_ext^2
c                        (last term added out of data loop)            
c
c Split-internal case: -ln L = -sum_i ln p(v_i|R_i) - sum_i w_i ln p_0(R_i)
c
            if (splitpflag .eq. 0) then
               lnlik(n) = lnlik(n) - wt(nd)*lnpfinal(nd)
            else if (sigltracerradius(n) .gt. 0.d0) then  ! fold in prior on tracer radius
               ! ignores weights
               lnlik(n) = lnlik(n) - lnpfinal(nd) 
c$$$     &          + 0.5d0*((ltracerradius(n)-meanltracerradius(n))**2.d0/
c$$$     &          sigltracerradius(n))**2.d0
            else ! solve also for r_nu given {R}
                 ! weights only applied to radii
               lnlik(n) = lnlik(n) - lnpfinal(nd) 
     &          - wt(nd)*lnp0(nd)
            endif
         enddo  ! end of data loop
c
c sum log-likelihoods over components
c
         mamposstlnlik = mamposstlnlik + lnlik(n)
         if (sigltracerradius(n) .gt. 0.d0 .and. splitpflag .ne. 0) then
            mamposstlnlik = mamposstlnlik
     &          + 0.5d0*(ltracerradius(n)-meanltracerradius(n))**2.d0/
     &           sigltracerradius(n)**2.d0
         endif 
          
      enddo            ! end of 2nd (big) loop over tracer components
c
c add Gaussian prior on concentration-mass relation
c
      if (siglc .gt. 0.d0 .and. darknormflag .le. SMALL) then
         mamposstlnlik = mamposstlnlik
     &              + 0.5d0*(dlog10(c)-lcmean)**2.d0 / siglc**2.d0
      endif
c
      if (debug .ge. 1) then
         if (anisflag .eq. 0) then
            print *,' latr(comp) lanis0(comp) lanisinf(comp) norm ',
     &      'darkscale lnlik:'
         else if (anisflag .eq. 1) then
            print *,' latr(comp) beta0(comp) betainf(comp) norm ',
     &           'darkscale lnlik:'
         else if (anisflag .eq. 2) then
            print *,' latr(comp) betasym0(comp) betasyminf(comp) norm ',
     &      'darkscale lnlik:'
         end if
         write (*,*) (ltracerradius(n),n=1,ncomp), 
     &    (lanis0(n),n=1,ncomp), (lanisinf(n),n=1,ncomp), 
     &    norm, darkscale,
     &    mamposstlnlik
      endif

      if (debug .ge. 4) stop
c
      return
      end
c
c END OF MAMPOSSTLNLIK
c
c
c
      double precision function g(rr,avz,evz,mu,emu)
c
c G returns model density in projected phase space normalized as
c
c 2 pi a_tr^3/[R N~_tr(a_tr)] g(R,v_z) (mu < 0)
c
c pi a^2 g / [N(a 
c
c dimension: 1/velocity
c
      implicit none
      real*8 rr, avz, evz, mu, emu
c
      include 'Rvz.i'
      include 'model2.i'
      include 'ilop.i'
      include 'integ.i'
      include 'debug.i'
      integer ier, i, imax
      integer neval, limit, lenw, last, iwork(MAXSPLITINTEG)
      real*8 umin, umax, aerr, rerr, aerror, arccosh, mass
      real*8 gint, work(4*MAXSPLITINTEG)
      real*8 ginteg, u
      external ginteg
c
c in case of joint halo+interloper, g=0 for R < r_vir
c
      if (rr .gt. rvir .and. mod(ilopflagg,2) .eq. 0) then 
         g = 0.d0
         return
      endif
c
c otherwise, must have r_max > R
c
      if (rr .gt. rmaxx) then
         print *,' WARNING: R = ', rr, ' > rmax = ', rmaxx
         g = 0.d0
         return
      endif
c
      rr1 = rr
      avz1 = avz
      evz1 = evz
      mu1 = mu
      emu1 = emu
c
      if (mu .gt. 10.d0 .and. distflagg .gt. 0) then
         umin = mu - 3.d0*emu
         umax = mu + 3.d0*emu
      else
         umin = 0.d0
         umax = arccosh(rmaxx/rr)
      endif 
      aerr = 0.d0
      rerr = 1.d-4
C$$$      if (rr > 1000.99d0) then
C$$$         print *,' G: umin umax = ', umin, umax
C$$$      endif 
      if (debugg .ge. 2) then
         print *,' G: R umin umax = ', sngl(rr), sngl(umin), sngl(umax)
      endif 
c
c compute integral
c
      limit = 500
      lenw = 4*limit
c$$$      call dqags(ginteg,umin,umax,aerr,rerr,g,aerror,
c$$$  &     neval,ier,limit,lenw,last,iwork,work)
      call dgaus8(ginteg,umin,umax,rerr,g,ier)
      if (ier. ne. 1) then
         print *,' integral of GINTEG (line-of-sight) has problems'
         call gaus8err(ier)
      end if
      rewind(40)
      rewind(140)
      if (debugg .ge. 2) then
         print *,' G: R umin umax rmax g = ', sngl(rr),
     &    sngl(umin), sngl(umax), sngl(rmaxx), sngl(g)
      endif
c
c check for errors in integration
c
      if (ier .lt. 0 .or. ier .gt. 2) then
         print *,' G: ier=', ier
         imax = 11
         print *,' R = ', sngl(rr), ' avz = ', sngl(avz)
         debugg = 5
         do i = 1, imax
            u = umin + dfloat(i-1)/dfloat(imax-1)*(umax-umin)
            gint = ginteg(u)
            print *,' u r ginteg = ', sngl(u), 
     &       sngl(rr*cosh(u)), sngl(gint)
         enddo 
         stop
      endif
c
      return
      end
c
c
c
      double precision function ginteg(u)
c
c GINTEG is a kernel used to estimate the density in projected phase space g(R,v_z)
c
c dimension: 1/velocity
c
      implicit none
      real*8 u
c
      integer nrb, ier
      integer icdum, kdum
      real*8 chu, r, lnrdum, lsigr, sigz
      real*8 density, betaprofile
      real*8 ginteg1, hofvz, mu, z, dmod2d, gaussian
      real*8 beta0, betainf, anisradius
      real*8 dd, gg, hh, par2
      real*8 nu
      character*16 anismodel
c
c common block
c
      include 'Rvz.i'
      include 'csig40.i'
      include 'model2.i'
      include 'debug.i'
      integer i, j, icsigr2, nrbins2, kdimsigr2
      real*8 lnr2(MAXRBINS), lsigmar2(MAXRBINS)
c      real*8 csigr2(MAXRBINS,3)
c$$$      real*8 csigr2(4,MAXRBINS), breaksigr2(MAXRBINS), ppvalu
      real*8 yy1, yy2, ypp2(MAXRBINS)
c
      if (mu1 .gt. 10.d0 .and. distflagg .gt. 0) then
         mu = u
         z = 1.d3*dmod2d(mu00)*(10.d0**(0.2d0*(mu-mu00))-1.d0)
         ! mu00 is the distance modulus to the full system
         r = dsqrt(rr1*rr1+z*z)
      else
         chu = dcosh(u)
         r = rr1*chu
      endif
      lnrdum = dlog(r)
c
c anisotropy parameters from common block
c
      anismodel = anismod
      anisradius = anisrad
      beta0 = bet0
      betainf = betinf
c
c copy spline parameters from common block
c
      nrbins2 = nrbins
      do nrb = 1, nrbins
         lnr2(nrb) = lnr(nrb)
         lsigmar2(nrb) = lsigmar(nrb)
         ypp2(nrb) = ypp(nrb)
c     print *,' GINTEG: nrb lnr2 c1 = ', nrb, lnr2(nrb),csigr2(nrb,1)
         if (debugg .ge. 2) then
c     print *,' lnr lsigr = ', sngl(lnrdum), sngl(lsigr)
            write (140,*) sngl(rr1), nrb, sngl(lnr(nrb)), 
     &        sngl(lsigmar(nrb))
         end if
      enddo
c
c interpolate sigma_r(r)
c
c$$$      lsigr = PPVALU(breaksigr2,csigr2,icsigr2,kdimsigr2,lnrdum,0)
      call SPLINE_CUBIC_VAL(nrbins2,lnr2,lsigmar2,ypp2,lnrdum,lsigr,
     &  yy1,yy2)
c
      sigz = 10.d0**lsigr*
     &  dsqrt(1.d0-betaprofile(r,beta0,betainf,anisradius,anismodel)
     &   *(rr1/r)**2.d0)
c
c add error in quadrature
c (equivalent to convolving by Gaussian of width evz1 outside integral, 
c  when hofvz is Gaussian too)
c
c set minimum sigz to avoid problems when err_vz > sigma_z!!!
c
c *** GAM 25 Mar 2016: I don't understand previous line nor sigz=0.1sigz below!!! ***
c
      if (evz1 .ge. sigz) then
         sigz = 0.1d0*sigz
      else
         sigz = dsqrt(sigz*sigz+evz1*evz1)
      endif
c$$$      ginteg = chu*density(r/atracer,tracermod)/sigz*
c$$$     &  dexp(-0.5d0*avz1*avz1/(sigz*sigz))
c      print *,' GINTEG: u vz sigz = ', sngl(u), sngl(avz1), sngl(sigz)

      par2 = tracerparam2
      dd = density(r/atracer,tracermod,par2)
      if (mu1 .gt. 0.d0 .and. distflagg .gt. 0) then
         gg = gaussian(mu,mu1,emu1)
         hh = hofvz(avz1,sigz,v3dmod)
         if (distflagg .eq. 1) then
            ginteg = gaussian(mu,mu1,emu1)*hofvz(avz1,sigz,v3dmod)
         else
            ginteg = gaussian(mu,mu1,emu1)*hofvz(avz1,sigz,v3dmod)
     &       *density(r/atracer,tracermod,par2)
         endif   
      else
         nu = density(r/atracer,tracermod,par2)
         hh = hofvz(avz1,sigz,v3dmod)
         ginteg = chu*density(r/atracer,tracermod,par2)*
     &    hofvz(avz1,sigz,v3dmod)
      endif 
      if (debugg .ge. 2) then
         print *,' GINTEG: r lsigr sigz ginteg = ',
     &      sngl(r), sngl(lsigr), sngl(sigz), sngl(ginteg)
         write(40,*) sngl(rr1), sngl(u), sngl(r), sngl(ginteg)
     &      , sngl(chu), sngl(nu), sngl(hh)
      endif
      if (debugg .ge. 5) then
         if (mu1 .gt. 10.d0 .and. distflagg .gt. 0) then
            print *,' GINTEG mu mu1 emu1 gaussian = ', sngl(mu),
     &       sngl(mu1), sngl(emu1), gg
         else
            print *,' GINTEG: u nu chu h sigz = ', sngl(u), sngl(dd), 
     &        sngl(chu), sngl(hh), sngl(sigz)
            print *,' GINTEG: beta0 betainf beta sigr = ', sngl(beta0), 
     &        sngl(betainf), sngl(betaprofile(r,beta0,betainf,
     &        anisradius,anismodel)), sngl(10.d0**lsigr)
         endif 
      endif

c$$$      write(*,99) u, r, avz1, sigz, hofvz(avz1,sigz,v3dmod)
c$$$ 99   format(' GINTEG: u r vz sigz h = ',f5.2,1x,f7.1,1x,f6.1,1x,f6.1,1x
c$$$     & , g13.5)
c$$$     &  dexp(-0.5d0*avz1*avz1/(sigz*sigz))
C$$$      print *,' r lsigr(1) anispar =', r,lsigr(1), anispar
C$$$      print *,' chu dens sigz sigr  = ', sngl(chu), 
C$$$     & sngl(density(r/atracer,tracermod)), sngl(sigz), 
C$$$     & sngl(10.d0**lsigr(1))
C$$$      if (dabs(rr1/3.708-1.d0) .lt. 1.d-3 .and. 
C$$$     &  dabs(avz1/2652-1.d0) .lt. 1.d-3) then
C$$$         write(99,*) ' GINTEG: r chu sigr sigz ginteg = ', r, chu, 
C$$$     &   10.d0**lsigr(1), sigz, ginteg
C$$$      endif
C$$$      if (rr1 .gt. 1000.99d0) then
C$$$         print *,' rr1 sigz ginteg = ', rr1, sigz, ginteg
C$$$      endif  
c
      return
      end
c
c
c
      double precision function hofvz(avz,sigz,v3dmodel)
c
c HOFVZ returns the distribution of line-of-sight velocities for given R and r
c R and r are hidden in sigz = sigma_z(R,r) = sigma_r(r) sqrt[1-beta(r) R^2/r^2]
c

      real*8 avz, sigz, gaussian
      character*16 v3dmodel
      include 'csts.i'
c
      if (v3dmodel .eq. 'Gaussian') then
         hofvz = gaussian(avz,0.d0,sigz)
      else
         print *,' HOFVZ: cannot recognize v3dmodel=', v3dmodel
         stop
      endif
c
      return
      end
c
c
c
      double precision function density(x,model,par2)
c
c DENSITY returns dimensionless 3D number density
c args: r/r_{-2} [NFW|gNFW|cNFW|Hernquist|gHernquist|Kazantzidis|Plummer|gPlummer] 
c    or r/a [Jaffe]
c    or r/Re [PrugnielSimien],
c    or r/r_fiducial [powerlaw], 
c  model (NFW|cNFW|gNFW|Hernquist|gHernquist|Jaffe|Kasantzidis|gKazantzidis|Plummer|gPlummer|mHubble|PrugnielSimien,PSinf,powerlaw)
c     units: M(r_-2)/(4 pi r_-2^3) [NFW|Hernquist],
c     M(R_e)/( 4 pi R_e^3) [PrugnielSimien,Sersicm8]
c     Minf  /( 4 pi R_e^3) [PSinf]
c
      implicit none
      real*8 x, par2
      integer i, ier
      character*16 model
      real*8 x1, sum, lgx, coeffs(0:6), sbr0
      real*8 bser, pser, m3mp, mass, bsersic, psersic
      real*8 arg, gam3, gam2, gam5
      real*8 DGAMLN, lngamma, DGAMI
      real*8 hya, hyb, hyc, hyx, hypgeom
      real*8 asinh
      include 'specfuns.i'
      include 'debug.i'
c
      if (x .le. 0.d0) then
         print *,' DENSITY: x = ', x, ' must be > 0!'
         stop
      endif
c
      x1 = x+1.d0
      if (model .eq. 'NFW') then
         density = 1.d0/(x*x1*x1)
         density = density/(dlog(2.d0)-0.5d0)
      else if (model .eq. 'cNFW') then
         density = 8.d0/(1.d0+2.d0*x)**3.d0/(dlog(3.d0)-8.d0/9.d0)
      else if (model .eq. 'gNFW') then
         gam2 = par2+2.d0
         gam3 = par2+3.d0
         hya = gam3
         hyb = gam3
         hyc = par2+4.d0
         hyx = -1.d0*gam2
         call HYGFX(hya,hyb,hyc,hyx,hypgeom)
         density = gam3*x**par2/(1.d0+gam2*x)**gam3/hypgeom
      else if (model .eq. 'sNFW') then
         density = 25.d0/(75.d0-18.d0*sqrt(15.d0))
     &     / (x*(1.d0+2.d0*x/3.d0)**2.5d0)
      else if (model .eq. 'Hernquist') then
         density = 36.d0/x/(x+2.d0)**3.d0
      else if (model .eq. 'gHernquist') then
         if (par2 .le. -3.d0) then
            print *,'DENSITY: cannot handle gHernquist model ',
     &        'with gamma = ', par2
            stop
         endif
         density = 2.d0*(3.d0+par2)*(4.d0+par2)**(3.d0+par2)*x**par2
     &        /((par2+2.d0)*x+2.d0)**(par2+4.d0)
      else if (model .eq. 'isothermal') then
         density = 1.d0/(1.d0-0.25*dacos(-1.d0))/(x*x+1.d0)
      else if (model .eq. 'Jaffe') then
         density = 2.d0/(x*x*x1*x1)
      else if (model .eq. 'Kazantzidis') then
         density = dexp(-x)/x/(1-2.d0/dexp(1.d0))
      else if (model .eq. 'gKazantzidis') then
         gam2 = par2+2.d0
         gam3 = par2+3.d0
         density = gam2**gam3 * x**par2 * dexp(-gam2*x) 
     &      / DGAMI(gam2,gam3)
      else if (model .eq. 'Plummer') then
         density = 45.d0*sqrt(5.d0)/(2.d0*x*x+3.d0)**2.5d0
      else if (model .eq. 'gPlummer') then ! units of M(a)/[4 pi a^3]
         ! r_{-2}/a = sqrt(gamma+2)/3
         gam3 = par2+3.d0
         gam5 = par2+5.d0
         density = 3.d0*gam3*x**par2*gam5**(0.5d0*gam3)
     &    /((par2+2.d0)*x*x+3.d0)**(0.5d0*gam5)
      else if (model .eq. 'mHubble') then
         density = dsqrt(8.d0)/(asinh(dsqrt(2.d0))-dsqrt(2.d0/3.d0))
     &   / (1.d0+2.d0*x*x)**1.5d0
      else if (model .eq. 'Sersicm07') then
         coeffs(0) = -0.318702d0
         coeffs(1) = -0.25151d0
         coeffs(2) = -0.0437304d0
         coeffs(3) = 0.0153157d0
         coeffs(4) = 0.00327989d0
         coeffs(5) = -0.00161455d0
         coeffs(6) = -0.000395832d0
         sum = 0.d0
         if (x .lt. 0.001d0 .or. x .gt. 1000.d0) then
            print *,' x = ', x
            print *,' Sersic m8 profile not known outside 0.001-1000 Re'
            stop
         endif
         lgx = dlog10(x)
         do i = 0, 6
            sum = sum + coeffs(i)*lgx**dfloat(i)
         enddo
         sbr0 = 2.87515d0
         density = 4.d0*sbr0*10.d0**sum
     &     *dexp(-bsersic(1.d0/dsqrt(2.d0))*x**dsqrt(2.d0))
       else if (model .eq. 'Sersicm8') then
         coeffs(0) = -7.10672d0
         coeffs(1) = -2.88296d0
         coeffs(2) = -0.283194d0
         coeffs(3) = -0.0269369d0
         coeffs(4) = -0.00201404d0
         coeffs(5) = -0.00011579d0
         sum = 0.d0
         if (x .lt. 0.001d0 .or. x .gt. 1000.d0) then
            print *,' x = ', x
            print *,' Sersic m8 profile not known outside 0.001-1000 Re'
            stop
         endif
         lgx = dlog10(x)
         do i = 0, 5
            sum = sum + coeffs(i)*lgx**dfloat(i)
         enddo
         sbr0 = 1.43425d6
         density = 4.d0*sbr0*10.d0**sum
     &     *dexp(-bsersic(1.d0/dsqrt(2.d0))*x**dsqrt(2.d0))
      else if (model .eq. 'PrugnielSimien'
     &        .or. model .eq. 'PSinf') then         
         bser = bsersic(par2)
         pser = psersic(par2)
         m3mp = par2*(3.d0-pser)
         if (debugg .ge. 4) then
            print *,' DENSITY: m3mp = ', m3mp
         endif   
         if (model .eq. 'PrugnielSimien') then
            density = bser**m3mp * dexp(-bser*x**(1.d0/par2)) / x**pser
     &           / (par2 * DGAMI(m3mp,bser))
         else                   ! normalize to mass at infinity
            lngamma = DGAMLN(m3mp,ier)
            if (ier .gt. 0) then
               print *,' DENSITY: m3mp = ', m3mp, ' lngamma = ', 
     &           lngamma, ' cannot compute Gamma(m3mp)'
               stop
            end if
            density = bser**m3mp * dexp(-bser*x**(1.d0/par2)) / x**pser
     &           / (par2 * dexp(lngamma))
         endif
         
      else if (model .eq. 'powerlaw') then
         density = (par2+3.d0) * x**par2
      else
         print *,' DENSITY: cannot recognize model = ', model
         stop
      endif
c
      return
      end
c
c
c
      double precision function mass(x,model,par2)
c
c MASS returns dimensionless mass at radius r
c args: r/r_{-2}  [NFW|cNFW|gNFW|Burkert|Einasto|Hernquist|Kazantzidis||gKazantzidis|Plummer]
c  or r/a [Jaffe] 
c  or r/Re [SersicmN]
c unit: M(r_{-2}) [NFW|cNFW|gNFW|Burkert|Einasto|Hernquist|Kazantizidis|gKazantzidis|Plummer] 
c  or M(a) [Jaffe, isothermal]
c  or M(Re) [PrugnielSimen|SersicmN]
c  or Minf  /( 4 pi R_e^3) [PSinf]
c  or M(r_fid) [powerlaw]
c
      implicit none
      real*8 x, par2
      integer i, ier
      character*16 model
      real*8 x1, sum, lgx, coeffs(0:8), MinfoverMofRe
      real*8 bser, pser, m3mp, bsersic, psersic
      real*8 DGAMLN, lngamma, DGAMI
      real*8 gam2
      real*8 gam3, hya, hyb, hyc, xarg, hypergeom
      real*8 mnfw, lgmnfwratio, mass2, x2plus1
      real*8 xx, asinh, rminus2overa, x2, x23p1, e
c
      include 'specfuns.i'
      include 'debug.i'
c
      if (x .lt. 0.d0) then
         print *,' MASS: x = ', x, ' must be >= 0!'
         stop
      endif
c
      x1 = x+1.d0
      if (debugg .ge. 4) then
         print *,' MASS: model = ', model,' ENDOFSTRING'
      endif 
      if (model .eq. 'NFW') then
         mass = (dlog(x1)-x/x1)/(dlog(2.d0)-0.5d0)
      else if (model .eq. 'cNFW') then
         x2plus1 = 2.d0*x+1.d0
         mass = (dlog(x2plus1)-2.d0*x*(3.d0*x+1.d0)/x2plus1**2.d0)
     &    / (dlog(3.d0)-8.d0/9.d0)
      else if (model .eq. 'gNFW') then
         ! NFW with free inner slope
         hya = par2 + 3.d0
         hyb = hya
         hyc = hya + 1.d0
         xarg = -1.d0*(par2+2.d0)*x
         call HYGFX(hya,hyb,hyc,xarg,hypergeom)
         ! careful that arguments can be changed on output
         mass = x**(par2+3.d0)*hypergeom/hypergeom1
c$$$         mnfw = (dlog(x1)-x/x1)/(dlog(2.d0)-0.5d0)
c         mass2 = mnfw*10.d0**lmnfwgenratio(dlog10(x),par2)
c         print *,' MASS: x gamma M_by2F1 M_ilop = ', sngl(x), 
c     &      sngl(par2), sngl(mass), sngl(mass2)
      else if (model .eq. 'NFWgenout' .or. model .eq. 'nfwgenout') then
         if (par2 .ge. -2.d0) then
            print *,' MASS: model = ', model, ' with par2 = ', par2
            print *,' par2 must be < -2!'
            stop
         else if (par2 .eq. -3.d0) then
            ! NFW
            mass = (dlog(x1)-x/x1)/(dlog(2.d0)-0.5d0)
         else 
            gam2 = par2 + 2.d0
c$$$            mass = (1.d0-(x+1.d0)*(1.d0-x/gam2)**gam2)
c$$$     &      /(1.d0-2.d0*(1.d0-1.d0/gam2)**gam2)
            xx = -x/gam2
            mass = 1.d0 + (gam2*xx-1.d0)*(xx+1.d0)**gam2 
            mass = mass / (1.d0 - 2.d0*((par2+1.d0)/gam2)**gam2)
         endif
      else if (model .eq. 'sNFW') then
         x23p1 = 1.d0+2.d0/3.d0*x
         mass = (1.d0+0.5d0*(1.d0-3.d0*x23p1)/x23p1**1.50d0)
     &    /(1.d0-sqrt(108.d0/125.d0))
      else if (model .eq. 'Hernquist') then
         mass = 9.d0*(x/(x+2.d0))**2.d0
      else if (model .eq. 'gHernquist') then ! Hernquist with free inner slope
         if (par2 .le. -3.d0) then
            print *,'MASS: cannot handle HernquistGen model ',
     &        'with gamma = ', par2
            stop
         endif
         mass = ((4.d0+par2)*x/(2.d0+(2.d0+par2)*x))**(par2+3.d0)
      else if (model .eq. 'Jaffe') then
         mass = 2.d0*x/x1
      else if (model .eq. 'Plummer') then
         ! mass = (5.d0*x*x/(3.d0+2.d0*x*x))**1.5d0
         ! mass = (5.d0*x*x/(1+4.d0*x*x))**1.5d0
         mass = 5.d0*dsqrt(5.d0)*x*x*x/(2*x*x+3.d0)**1.5d0
      else if (model .eq. 'gPlummer') then ! Plummer with free inner slope
         gam2 = par2+2.d0
         gam3 = par2+3.d0
c$$$         mass = ((gam2*gam2+1.d0)*x*x/((gam2*x)**2.d0+1.d0))
c$$$     &    **(0.5d0*par2+1.5d0)
         mass = x**gam3*((par2+5.d0)/(gam2*x*x+3.d0))**(0.5d0*gam3)
      else if (model .eq. 'Kazantzidis') then
         e = dexp(1.d0)
         mass = e/(e-2.d0)*(1.d0-(1.d0+x)*dexp(-x))
      else if (model .eq. 'gKazantzidis') then
         gam2 = par2+2.d0
         mass = DGAMI(3.d0+par2,gam2*x)/DGAMI(3.d0+par2,gam2)
      else if (model .eq. 'mHubble') then
         xx = x*dsqrt(2.d0)
         mass = (asinh(xx)-xx/dsqrt(2.d0*x*x+1.d0))
     &   / (asinh(dsqrt(2.d0))-dsqrt(2.d0/3.d0))
      else if (model .eq. 'Burkert') then
         rminus2overa = 1.52138d0
         ! r_{-2}/a = [1-sqrt(26/27)]^1/3 + [1+sqrt(26/27)]^1/3
         xx = rminus2overa*x
         mass = dlog(1+xx*xx) + 2.d0*dlog(1+xx) - 2.d0*datan(xx)
         xx = rminus2overa
         mass = mass / 
     &      (dlog(1+xx*xx) + 2.d0*dlog(1+xx) - 2.d0*datan(xx))
      else if (model .eq. 'Einasto') then
         mass = DGAMI(3.d0*par2,2.d0*par2*x**(1.d0/par2))
     &    /DGAMI(3.d0*par2,2.d0*par2)
      else if (model .eq. 'isothermal') then
         mass = (x - datan(x)) / (1.d0 - 0.25d0*dacos(-1.d0))
      else if (model .eq. 'Sersicm07') then
         ! NOT SURE NORMALIZED AT 1!
         coeffs(0) = 0.332973d0
         coeffs(1) = -2.65253d0
         coeffs(2) = -0.590049d0
         coeffs(3) = -2.05429d0
         coeffs(4) = -2.79876d0
         coeffs(5) = -2.06349d0
         coeffs(6) = -0.844764d0
         coeffs(7) = -0.179816d0
         coeffs(8) = -0.0154989d0
         if (x .lt. 0.001d0) then
            print *,' x = ', x
            print *,' m=1/sqrt(2) Sersic mass profile not known',
     &       ' outside 0.001-1000 Re'
            stop
         endif
         lgx = dlog10(x)
         do i = 0, 8
            sum = sum + coeffs(i)*lgx**dfloat(i)
         enddo
         MinfoverMofRe = 3.15265d0
         mass = MinfoverMofRe/(1.d0+10.d0**sum)
      else if (model .eq. 'Sersicm8') then
         ! NOT SURE NORMALIZED AT 1!
         coeffs(0) = -0.00132162d0
         coeffs(1) = 0.443595d0
         coeffs(2) = -0.170005d0
         coeffs(3) = 0.0119011d0
         coeffs(4) = 0.00403418d0
         coeffs(5) = -0.000357494d0
         if (x .lt. 0.01d0 .or. x .gt. 1000.d0) then
            print *,' x = ', x
            print *,' m=8 Sersic mass profile not known outside',
     &       ' 0.01-1000 Re'
            stop
         endif
         lgx = dlog10(x)
         do i = 0, 5
            sum = sum + coeffs(i)*lgx**dfloat(i)
         enddo
         mass = 10.d0**sum
      else if (model .eq. 'PrugnielSimien'
     &  .or. model .eq. 'PSinf') then
         bser = bsersic(par2)
         pser = psersic(par2)
         m3mp = par2*(3.d0-pser)
         if (debugg .ge. 4) then
            print *,' MASS: m3mp = ', m3mp, ' m = ', par2, 
     &      ' x = ', x
            print *,' MASS: debugg = ', debugg
         endif
         if (model .eq. 'PrugnielSimien') then
            mass = DGAMI(m3mp,bser*x**(1.d0/par2))
     &          /DGAMI(m3mp,bser)
         else ! normalize to mass at infinity
            lngamma = DGAMLN(m3mp,ier)
            mass = DGAMI(m3mp,bser*x**(1.d0/par2))
     &          /dexp(lngamma)
         endif
      else if (model .eq. 'powerlaw') then
         mass = x**(par2+3.d0)
      else
         print *,' MASS: cannot recognize model=', model
         stop
      endif
      return
      end
c
c
c
      double precision function lmgNFWratio(lx,g)
c
c LMGNFWRATIO computes an order-12 2D Polynomial approximation
c to log_10 {[MgNFW(10^lx,gamma)/MgNFW(1,gamma)]/MNFW(x)}
c
c source: ~/IK/MNFWgenin_to400.nb
c accuracy for -2 < g < 0 and -2 < lX < 2.6:
c rms: 1.6e-4, max: 6e-4
c
      implicit none
      real*8 lx, g
      real*8 lx2, lx3, lx4, lx5, lx6, lx7, lx8, lx9, lx10, lx11, lx12
      real*8 g2, g3, g4, g5, g6, g7, g8, g9, g10, g11, g12
c
c check that we are within limits of fit
c
      if (g .lt. -1.9d0 .or. g .gt. 0.d0) then
         print *,' LMGNFWRATIO: gamma = ', g
         print *,' gamma must be between -1.9 and 0'
         stop
      endif
      if (lx .lt. -3.d0 .or. lx .gt. 2.6d0) then
         print *,' LMGNFWRATIO: lX = ', lX
         print *,' lX must be between -3 and 2.6'
         stop
      endif
c
c 12th order polynomial approximation (45 non-zero terms)
c
      lx2 = lx*lx
      lx3 = lx2*lx
      lx4 = lx2*lx2
      lx5 = lx4*lx
      lx6 = lx3*lx3
      lx7 = lx6*lx
      lx8 = lx4*lx4
      lx9 = lx8*lx
      lx10 = lx5*lx5
      lx11 = lx10*lx
      lx12 = lx6*lx6
      g2 = g*g
      g3 = g2*g
      g4 = g2*g2
      g5 = g4*g
      g6 = g3*g3
      g7 = g6*g
      g8 = g4*g4
      g9 = g8*g
      g10 = g5*g5
      g11 = g10*g
      g12 = g6*g6
      lmgnfwratio = 
     -  -0.18590037468439102 - 0.32023495405443075*g - 
     -  0.052124179098919046*g2 - 
     -  0.23887069554272017*g3 - 3.5442459523260483*g4 - 
     -  14.652093198381282*g5 - 33.49545272496791*g6 -
     -  47.83814171131207*g7 - 
     -  44.46041425125742*g8 - 26.921781767888632*g9 - 
     -  10.25129561493776*g10 - 2.229713993890536*g11 - 
     -  0.2113191924475706*g12 + 0.6909203180826227*lX + 
     -  0.7714643647452194*g*lX - 0.11741686577554367*g2*lX - 
     -  1.3096253505733668*g3*lX - 5.0750141977602485*g4*lX - 
     -  12.18674198271269*g5*lX - 18.78024345198703*g6*lX - 
     -  19.039227362367928*g7*lX - 12.63073643148159*g8*lX - 
     -  5.278961221895685*g9*lX - 1.2609309138880795*g10*lX - 
     -  0.13125316000491358*g11*lX - 0.23611452522855592*lX2 - 
     -  0.24916344803989038*g*lX2 - 0.25399904421414593*g2*lX2 - 
     -  1.0017124559897312*g3*lX2 - 2.262505654384724*g4*lX2 - 
     -  3.452976527382423*g5*lX2 - 3.6056772008087052*g6*lX2 - 
     -  2.5362454944075936*g7*lX2 - 1.151353115988836*g8*lX2 - 
     -  0.30460873574065644*g9*lX2 - 0.035763047943081765*g10*lX2 - 
     -  0.050807852289695456*lX3 - 0.06601135298346227*g*lX3 + 
     -  0.012140235212024694*g2*lX3 + 0.07970007447540693*g3*lX3 + 
     -  0.17249444346896384*g4*lX3 + 0.2762615776761082*g5*lX3 + 
     -  0.2578302171150118*g6*lX3 + 0.14007826720111685*g7*lX3 + 
     -  0.04048016411942875*g8*lX3 + 0.004719206595783611*g9*lX3 + 
     -  0.03250882880018291*lX4 + 0.04114569448567341*g*lX4 + 
     -  0.08089096590959081*g2*lX4 + 0.20368104827562505*g3*lX4 + 
     -  0.27715885873618185*g4*lX4 + 0.24601112699973363*g5*lX4 + 
     -  0.14065294829362368*g6*lX4 + 0.0463044683257199*g7*lX4 + 
     -  0.006821336163599657*g8*lX4 + 0.012865752899268995*lX5 + 
     -  0.013229931565546254*g*lX5 - 0.0022090999973848357*g2*lX5 - 
     -  0.0009473986474057473*g3*lX5 + 0.007426853735996355*g4*lX5 + 
     -  0.00602214626142993*g5*lX5 + 0.0029159225406631714*g6*lX5 + 
     -  0.0006763796020098637*g7*lX5 - 0.0048132972873546585*lX6 - 
     -  0.00642780429703938*g*lX6 - 0.010627329363745814*g2*lX6 - 
     -  0.018012653916575107*g3*lX6 - 0.013846283757753469*g4*lX6 - 
     -  0.005643935336233612*g5*lX6 - 0.001137103363808436*g6*lX6 - 
     -  0.002100827822107714*lX7 - 0.0016086775953059634*g*lX7 + 
     -  0.0004009106265161264*g2*lX7 + 0.00019325364682707546*g3*lX7 - 
     -  0.000529314828892436*g4*lX7 - 0.0001826687852075434*g5*lX7 + 
     -  0.0004144023004839365*lX8 + 0.0005267666932877581*g*lX8 + 
     -  0.0005226362124352523*g2*lX8 + 0.0005827844529408126*g3*lX8 + 
     -  0.0002108535624271963*g4*lX8 + 0.00019035329311844714*lX9 + 
     -  0.00010700824456650534*g*lX9 - 0.000023735578601860233*g2*lX9 - 
     -  0.00002647376700058309*g3*lX9 - 0.000011525583764272479*lX10 - 
     -  0.000016321028761366978*g*lX10 - 3.2742292117230537e-6*g2*lX10 - 
     -  7.241909761108451e-6*lX11 - 2.965963916907012e-6*g*lX11 - 
     -  3.389267995147521e-7*lX12
      return
      end
c
c
c
      double precision function surfdensity(X,model,par2)
c
c SURFDENSITY returns dimensionless surface density at projected radius R
c     args: R/r_-2 [NFW|Hernquist] or R/Re [Sersicm8] or R/r_1 [powerlaw],
c      model (NFW|Hernquist|Sersicm8)
c     units: M(r_-2)/(pi r_-2^2) [NFW|Hernquist]
c     or M(Re)/(pi Re^2) [Sersicm8]
c     or Minf/(pi Re^2) [PSinf]
c     or M(r_1)/(pi r_1^2) [powerlaw]
c
      implicit none
      real*8 X, par2
      character*16 model
      integer i, ier
      real*8 X2, Xmin1, aX2min1, sqaX2min1, TINY, aco, ase, bsersic
      real*8 bser, lngamma, DGAMLN
      real*8 sbr0, lsbr0, m, pi
      real*8 hya, hyb, hyc, hyx, hypgeom1, hypgeom2
      real*8 DGAMMA
      real*8 asinh
      real*8 g, g2, g3, g4, g5, X4, g2X2, g2X2plus3
      real*8 coeffs(0:8)
      parameter (TINY=1.d-20)
      include 'model2.i'
c
      if (X .lt. 0.d0) then
         print *,' SURFDENSITY: X = ', X, ' must be >= 0!'
         stop
      endif
c
      pi = dacos(-1.d0)
      X2 = X*X
      Xmin1 = X-1.d0
      aX2min1 = dabs(X2-1.d0)
      sqaX2min1 = dsqrt(aX2min1)

      if (model .eq. 'NFW') then
        if (dabs(Xmin1) .gt. TINY) then
           surfdensity = (1.d0-aco(1.d0/X)/sqaX2min1)/(X2-1.d0)
        else if (X .gt. 0.d0) then
           surfdensity = 1.d0/3.d0
        endif
      else if (model .eq. 'Hernquist') then
        if (dabs(Xmin1) .gt. TINY) then
           surfdensity = 9.d0/(X2-4.d0)**2.d0
     &       * ((X2+8.d0)*ase(0.5d0*X)/dsqrt(dabs(X2-4.d0)) - 6.d0)
        else if (X .gt. 0.d0) then
           surfdensity = 0.3d0
        endif
      else if (model .eq. 'isothermal') then ! pseudo-isothermal
         surfdensity = pi/(4.d0-pi) / dsqrt(X2+1.d0)
      else if (model .eq. 'Jaffe') then
         if (dabs(Xmin1) .gt. TINY) then
            surfdensity = -2.d0*pi/X - 1.d0/(X2-1.d0) 
     &      - (X2-2.d0)/(X2-1.d0)/sqaX2min1
         else if (X .gt. 0.d0) then
            surfdensity = pi/2-4.d0/3.d0
         endif   
      else if (model .eq. 'Plummer') then
         surfdensity = 15.d0*dsqrt(2.5d0)/(3.d0+2.d0*x*x)**2.d0
      else if (model .eq. 'gPlummer') then
         ! tested w Mathematica
         g = par2
         g2 = g+2.d0
         g3 = g+3.d0
         g4 = g+4.d0
         g5 = g+5.d0
         X2 = X*X
         X4 = X2*X2
         g2X2 = g2*X2
         g2X2plus3 = g2X2+3.d0
         hya = 1.d0
         hyb = g5/2.d0
         hyc = -0.5d0
         hyx = -3.d0/(g2X2)
         call HYGFX(hya,hyb,hyc,hyx,hypgeom1)
         ! repeat, beacause HYGFX can change arguments on output
         hya = 1.d0
         hyb = g5/2.d0
         hyc = 0.5d0
         hyx = -3.d0/(g2X2)
         call HYGFX(hya,hyb,hyc,hyx,hypgeom2)
         surfdensity = g3*(g+5.d0)**(0.5d0*g3)
     &    /(6.d0*g4*g2**(0.5d0*(g+7.d0))*X4*g2X2plus3)
     &    *(9.d0*g4*g2*g2X2
     &      + g2X2plus3*(g2X2*g2X2-9.d0*g3*g3)*hypgeom1
     &      - (g2X2+3.d0*g3)*(g2X2*(g2X2+3.d0*(g+8.d0))
     &                        -9.d0*(17.d0+(g+9.d0)*g))*hypgeom2)
      else if (model .eq. 'mHubble') then
         surfdensity = 1.d0/
     &    (asinh(dsqrt(2.d0)) - dsqrt(2.d0/3.d0))
     &    / (2.d0*X*X+1.d0)
      else if (model .eq. 'cNFW') then
         X2 = X*X
         print *,' cNFW not implemented yet (for surface density)!'
         stop
      else if (model .eq. 'Sersicm07') then
         sbr0 = 2.87515d0
         surfdensity = sbr0*
     &     dexp(-bsersic(1.d0/dsqrt(2.d0))*X**dsqrt(2.d0))
      else if (model .eq. 'Sersicm8') then
         sbr0 = 1.43425d6
         surfdensity = sbr0*dexp(-bsersic(8.d0)*X**0.125d0)
      else if (model .eq. 'PrugnielSimien') then
         coeffs(0) = 0.27955d0
         coeffs(1) = -0.0375936d0
         coeffs(2) = 0.543377d0
         coeffs(3) = -0.213835d0
         coeffs(4) = 0.0528564d0
         coeffs(5) = -0.00813446d0
         coeffs(6) = 0.000754755d0
         coeffs(7) = -0.0000385725d0
         coeffs(8) = 8.32867e-7
         lsbr0 = 0.d0
         do i = 0, 8
            lsbr0 = lsbr0 + coeffs(i)*par2**dfloat(i)
         enddo
         sbr0 = 10.d0**lsbr0
         surfdensity = sbr0*dexp(-bsersic(par2)*X**(1.d0/par2))
      else if (model .eq. 'PSinf') then
         bser = bsersic(par2)
         lngamma = DGAMLN(par2+par2,ier)
         surfdensity = bser**(2.d0*par2)/(2.d0*par2*dexp(lngamma))
     &     * dexp(-bser*X**(1.d0/par2))
      else if (model .eq. 'powerlaw') then
         if (par2 .le. -3.d0 .or. par2 .ge. -1.d0) then
            print *,' SURFDENSITY: par2 must be in ]-3,-1[!'
            stop
         endif
         surfdensity = 0.25d0*dsqrt(pi) * (par2+3.d0)
     &        * DGAMMA(-0.5d0*(par2+1.d0))/DGAMMA(-0.5d0*par2)
     &        * X**(par2+1.d0)
      else
         print *,' SURFDENSITY: cannot recognize model = ', model,
     &   'END-OF-STRING'
      endif
c
      return
      end
c
c
c
      double precision function bsersic(m)
c      
c b-parameter of Sersic profile
c where b^m = R_eff/a      
c      
      implicit none
      real*8 m
c
c source: Ciotti & Bertin (1999)
c      
      bsersic = 2.d0*m - 1.d0/3.d0 +  4.d0/(405.d0*m) + 
     &  46.d0/(25515.d0*m**2) + 131.d0/(1.148175d6*m**3) -
     &  2194697.d0/(3.069071775d10*m**4.d0)
      return
      end
c
c
c
      double precision function psersic(m)
c
c inner slope of deprojected Sersic (PrugnielSimien)
c
      implicit none
      real*8 m
c
c source: Lima-Neto, Gerbal & Marquez 1998
c
      psersic = 1.d0-0.6097d0/m+0.05463d0/(m*m)
c
      return
      end
c
c
c
      double precision function massproj(X,model,par2)
c
c MASSPROJ returns the dimensionless projected mass at radius R
c args: R/a model (NFW|Hernquist)
c unit: M(a)
c     Plummer & Plummergen use r_{-2} units
c PSinf use mass at infinity      
c others (Hernquist, Hernquistgen, NFWgen, Kazgen) need to be updated!
c
      implicit none
      real*8 X, par2
      character*16 model
      integer i
      real*8 X2, Xmin1, aX2min1, sqaX2min1, TINY, aco, ase, Xinv, pi
      real*8 m, m2, b, sbr0, bsersic, gam2m, bser, lsbr0
      real*8 mass, DGAMI, DGAMMA, DGAMLN
      real*8 g, g2, g3, g5, g2X2, g1over2, g3over2, g5over2, g2X2plus3
      real*8 m1, m3, m4, m5, m6, m7
      real*8 hya, hyb, hyc, hyx, hypgeom1, hypgeom2, gammag5over2
      real*8 asinh
      real*8 coeffs(0:8)
      parameter (TINY=1.d-20)
      ! include 'model2.i'
      include 'debug.i'
c
      pi = dacos(-1.d0)
      X2 = X*X
      Xmin1 = X-1.d0
      aX2min1 = dabs(X2-1.d0)
      sqaX2min1 = dsqrt(aX2min1)
      Xinv = 1.d0/X
C$$$      print *,' MASSPROJ: model = ', model, ' X = ', X
      if (X .lt. 0.d0) then
         print *,' MASSPROJ: X = ', X, ' must be >= 0!'
         stop
      endif
c
      if (X .eq. 0.d0) then
         massproj = 0.d0
      else if (model .eq. 'NFW') then
         if (dabs(Xmin1) .gt. 0.0001d0) then
            massproj = aco(Xinv)/sqaX2min1+dlog(0.5d0*X)
         else
            massproj = 1.d0-dlog(2.d0) + (X-1.d0)/3.d0
         endif
         massproj = massproj/(dlog(2.d0)-0.5d0)
C$$$         print *,' in NFW: massproj = ', massproj
      else if (model .eq. 'cNFW') then
         ! X2 = X*X
         print *,' MASSPROJ: cNFW not implemented yet!'
         stop
      else if (model .eq. 'Hernquist') then
         if (dabs(Xmin1) .gt. TINY) then
            massproj = 9.d0*X2/(X2-4.d0) *
     &       (1.d0 - 2.d0*ase(0.5d0*X)/dsqrt(dabs(X2-4.d0)))
         else
            massproj = 3.d0
         endif
      else if (model .eq. 'isothermal') then
         massproj = (dsqrt(X2+1.d0)-1.d0) / (2.d0/pi - 0.5d0)
      else if (model .eq. 'Jaffe') then
         if (dabs(Xmin1) .gt. TINY) then
            massproj = pi*X - 2.d0*X2*ase(X)/sqaX2min1
         else
            massproj = pi - 2.d0
         endif
      else if (model .eq. 'Plummer') then
         ! units of r_{-2}
         massproj = X*X/(X*X+1.d0)
      else if (model .eq. 'gPlummer') then
         ! units of r_{-2}
         X2 = X*X
         g = par2
         g1over2 = (g + 1.d0)/2.d0
         g2 = g + 2.d0
         g2X2 = g2*X2
         g3 = g + 3.d0
         g5 = g + 5.d0
         g3over2 = 0.5d0*g3
         g5over2 = 0.5d0*g5
         g2X2plus3 = g2X2 + 3.d0
         hya = 1.d0
         hyb = g5over2
         hyc = -0.5d0
         hyx = -3.d0/(g2*X*X)
         Gammag5over2 = DGAMMA(g5over2)
         call HYGFX(hya,hyb,hyc,hyx,hypgeom1)
         ! repeat, beacause HYGFX can change arguments on output
         hya = 1.d0
         hyb = g5over2
         hyc = 0.5d0
         hyx = -3.d0/(g2*X*X)
         call HYGFX(hya,hyb,hyc,hyx,hypgeom2)
         m1 = (g5*X2/g2X2plus3)**g3over2 
         m2 = g3*(g5/3.d0)**g3over2 / (2.d0*Gammag5over2)
         m3 = 2.d0*3.d0**g1over2*Gammag5over2
     3    /((g+4.d0)*g2**((g+7.d0)/2.d0)*X2)
         m4 = g2*g2*X2*(X2-3.d0) - 9.d0*g3
         m5 = g2X2*(g2X2+3.d0*(g+8.d0))-9.d0*(g3*(g+6.d0)-1.d0)
         m6 = (3.d0/g2)**g3over2*DGAMMA(g3over2)/g2X2plus3
     7     *(g2*((g2/g2X2plus3)**g1over2*X**g3 - X2) - 3.d0)
         m7 = m4*hypgeom1-m5*hypgeom2
         massproj = m1+m2*(m3*m7-m6)
      else if (model .eq. 'mHubble') then
         massproj = 0.5d0/
     &    (asinh(dsqrt(2.d0)) - dsqrt(2.d0/3.d0))
     &    *dlog(2.d0*X*X + 1.d0)
      else if (model .eq. 'PrugnielSimien') then
         coeffs(0) = 0.27955d0
         coeffs(1) = -0.0375936d0
         coeffs(2) = 0.543377d0
         coeffs(3) = -0.213835d0
         coeffs(4) = 0.0528564d0
         coeffs(5) = -0.00813446d0
         coeffs(6) = 0.000754755d0
         coeffs(7) = -0.0000385725d0
         coeffs(8) = 8.32867e-7
         lsbr0 = 0.d0
         m = par2
         do i = 0, 8
            lsbr0 = lsbr0 + coeffs(i)*m**dfloat(i)
         enddo
         sbr0 = 10.d0**lsbr0
         bser = bsersic(m)
         if (debugg .ge. 3) then
            print *,' 2m = ', 2.d0*m
         endif
         massproj = 2.d0*sbr0*m/bser**(2.d0*m)*
     &        DGAMI(2.d0*m,bser*X**(1.d0/m))
      else if (model .eq. 'PSinf') then ! Prugniel Simien normalized to mass at infinity
         massproj = DGAMI(par2+par2,bsersic(par2)*X**(1.d0/par2))
     &        / dexp(DGAMLN(par2+par2))
      else if (model .eq. 'powerlaw') then
         if (par2 .le. -3.d0 .or. par2 .ge. -1.d0) then
            print *,' SURFDENSITY: par2 must be in ]-3,-1[!'
            stop
         endif
         massproj = -0.25d0*dsqrt(pi) * (par2+3.d0)
     &        * DGAMMA(-0.5d0*(par2+3.d0))/DGAMMA(-0.5d0*par2)
     &          * X**(par2+3.d0)
      else
         print *,' MASSPROJ: cannot recognize model = ', model,
     &   'END-OF-STRING'
      endif
c
      return
      end
c
c
      double precision function massprojsph(X,c,model,par2)
c
c MASSPROJSPH returns the dimensionless projected mass at radius R
c args: R/a r_vir/a model (NFW|Hernquist) par2
c unit: M(a)
c
      implicit none
      real*8 X, c, par2
      character*16 model
      integer i
      real*8 X2, Xmin1, aX2min1, sqaX2min1, TINY, aco, ase, Xinv
      real*8 c1, sqrc2X2, DGAMI
      real*8 m, m2, b, sbr0, bsersic, gam2m, bser, lsbr0
      real*8 coeffs(0:8)
      parameter (TINY=1.d-20)
      include 'model2.i'
c
      X2 = X*X
      Xmin1 = X-1.d0
      aX2min1 = dabs(X2-1.d0)
      sqaX2min1 = dsqrt(aX2min1)
      Xinv = 1.d0/X
C$$$      print *,' MASSPROJSPH: model = ', model, ' X = ', X
      if (X .lt. 0.d0) then
         print *,' MASSPROJSPH: X = ', X, ' must be >= 0!'
         stop
      endif
c
      if (X .eq. 0.d0) then
         massprojsph = 0.d0
      else if (model .eq. 'NFW') then
         c1 = c+1.d0
         if (X .ge. c) then
            massprojsph = dlog(c1)-c/c1
         else if (dabs(Xmin1) .gt. TINY) then
            sqrc2X2 = dsqrt(c*c-X2)
c            print *,' MASSPROJSPH: c X = ', c, X
            massprojsph = (sqrc2X2-c)/c1 + dlog(c1*(c-sqrc2X2)/X)
     &       +aco((c+X2)/(c1*X))/dsqrt(dabs(1.d0-X2))
         else
            massprojsph = dlog(c1*(c-dsqrt(c*c-1.d0)))-c/c1
     &       + 2.d0*dsqrt((c-1.d0)/c1)
         endif
         massprojsph = massprojsph/(dlog(2.d0)-0.5d0)
C$$$         print *,' in NFW: massprojsph = ', massprojsph
C$$$      else if (model .eq. 'Hernquist') then
C$$$         if (dabs(Xmin1) .gt. TINY) then
C$$$            massprojsph = 3.d0*X2/aX2min1*dabs(ase(X)/sqaX2min1-1.d0)
C$$$         else if (X .eq. 1.d0) then
C$$$            massprojsph = 1.d0
C$$$         endif
C$$$      else if (model .eq. 'PrugnielSimien') then
C$$$         coeffs(0) = 0.27955d0
C$$$         coeffs(1) = -0.0375936d0
C$$$         coeffs(2) = 0.543377d0
C$$$         coeffs(3) = -0.213835d0
C$$$         coeffs(4) = 0.0528564d0
C$$$         coeffs(5) = -0.00813446d0
C$$$         coeffs(6) = 0.000754755d0
C$$$         coeffs(7) = -0.0000385725d0
C$$$         coeffs(8) = 8.32867e-7
C$$$         lsbr0 = 0.d0
C$$$         m = tracerparam2
C$$$         do i = 0, 8
C$$$            lsbr0 = lsbr0 + coeffs(i)*m**dfloat(i)
C$$$         enddo
C$$$         sbr0 = 10.d0**lsbr0
C$$$         bser = bsersic(m)
C$$$         massprojsph = 2.d0*sbr0*m/bser**(2.d0*m)*
C$$$     &    DGAMI(2.d0*m,bser*X**(1.d0/m))
      else
         print *,' MASSPROJSPH: cannot recognize model = ', model,
     &   'END-OF-STRING'
      endif
c
      return
      end
c
c
      double precision function massprojsec(X,model)
c
c MASSPROJSEC returns the dimensionless projected mass at radius R
c args: R/a model (NFW|Hernquist)
c unit: M(a)
c
      implicit none
      real*8 X
      character*16 model
      real*8 X2, Xmin1, aX2min1, sqaX2min1, TINY, aco, ase
      parameter (TINY=1.d-20)
c
      X2 = X*X
      Xmin1 = X-1.d0
      aX2min1 = abs(X2-1.d0)
      sqaX2min1 = dsqrt(aX2min1)
C$$$      print *,' MASSPROJ: model = ', model, ' X = ', X
      if (X .lt. 0.d0) then
         print *,' MASSPROJSEC: X = ', X, ' must be >= 0!'
         stop
      endif
c
      if (X .eq. 0.d0) then
         massprojsec = 0.d0
      else if (model .eq. 'NFW') then
         if (dabs(Xmin1) .gt. TINY) then
            massprojsec = ase(X)/sqaX2min1+dlog(0.5d0*X)
         else
            massprojsec = 1.d0-dlog(2.d0)
         endif
         massprojsec = massprojsec/(dlog(2.d0)-0.5)
C$$$         print *,' in NFW: massproj = ', massproj
      else if (model .eq. 'Hernquist') then
         if (dabs(Xmin1) .gt. TINY) then
            massprojsec = 3.d0*X2/aX2min1*dabs(ase(X)/sqaX2min1-1.d0)
         else if (X .eq. 1.d0) then
            massprojsec = 1.d0
         endif
      else
         print *,' MASSPROJSEC: cannot recognize model = ', model,
     &   'END-OF-STRING'
      endif
c
      return
      end
c
c
c
      double precision function betaprofile(r,beta0,betainf,anisradius,
     &  anismodel)
c
c BETAPROFILE returns beta(r) for given anisotropy models
c
      implicit none
      real*8 r, beta0, betainf, anisradius
      character*16 anismodel
c
      if (anismodel .eq. 'isotropic') then
         betaprofile = 0.d0
      else if (anismodel .eq. 'constant') then   ! constant
         betaprofile = beta0
      else if (anismodel .eq. 'OsipkovMerritt') then    ! Osipkov-Merritt
         betaprofile = r*r/(r*r+anisradius*anisradius)
      else if (anismodel .eq. 'gOsipkovMerritt') then ! Osipkov-Merritt with general inner and outer values
         betaprofile = beta0 + (betainf-beta0)*r*r/
     &    (r*r+anisradius*anisradius)
      else if (anismodel .eq. 'MamonLokas') then    ! Mamon-Lokas (2005b)
         betaprofile = 0.5d0*r/(r+anisradius)
      else if (anismodel .eq. 'DMS') then   ! Diemand, Moore & Stadel 
         betaprofile = dmin1(1.d0,(r/anisradius)**(1.d0/3.d0))
c$$$      else if (anismodel .eq. 'T' .or. anismodel .eq. 'Tiret') then
c$$$  betaprofile = betainf*r/(r+anisradius)
      else if (anismodel .eq. 'Tiret') then      ! Tiret et al. 07
         betaprofile = beta0 + (betainf-beta0)*r/(r+anisradius)
      else
         print *,' BETAPROFILE: cannot recognize model = ', anismodel
         stop
      endif
c
      return
      end
c
c
c
c$$$      double precision function totalmasstracers(r)
c$$$c
c$$$c TOTALMASSTRACERS returns the total mass of all tracers at radius r
c$$$c
c$$$      implicit none
c$$$      real*8 r
c$$$c
c$$$      integer n
c$$$c      
c$$$      totalmasstracers = 0.d0
c$$$      do n = 1, ncomp
c$$$         totalmasstracers = totalmasstracers +
c$$$     &     10.d0*ltracermass(n)
c$$$     &     *mass(r/atrac(n),tracermodel(n),tracerpar2(n))
c$$$     &     /mass(rfidtracer(n)/atrac(n),tracermodel(n),tracerpar2(n))
c$$$      enddo
c$$$c
c$$$      return
c$$$      end
c
c
c
      double precision function fnusigr2(lns)
c
c FNUSIGR2 returns nu(s)*v_circ^2(s)*Ksigma(r,s)/s for s = exp(lns)
c i.e. it solves Jeans equation
c
      implicit none
      real*8 lns
c
      integer n
      real*8 s, Ksigmarratio, density, mass, par2
      real*8 Mtr11, Md11, MBH, Mt, vc, K, Kpred, nuovers
      real*8 rm
      logical isnan
c
c common block
c
      include 'model2.i'
      include 'params.i'
      include 'components.i'
      include 'debug.i'
c
      s = dexp(lns)
      par2 = tracerparam2
      nuovers = density(s/atracer,tracermod,par2)/s
      K = Ksigmarratio(rad/anisrad,s/anisrad,anismod,bet0,betinf)
c$$$      if (isnan(K) .and. anisrad .gt. 0.d0 .and. beta0 .le. 1.d0 
c$$$     &  .and. betinf .le. 1.d0) then
c$$$         K = 0.d0
c$$$      endif

c      fnusigr2 = nuovers*K
c$$$      if (anismod .eq. 'cst') then ! avoids NaN when beta=cst<<0
c$$$         fnusigr2 = (s/rad)**(2.d0*bet0)*nuovers
c$$$      else   
c$$$         fnusigr2 = Ksigmar(s/anisrad,anismod,bet0,betinf)
c$$$     &      /Ksigmar(rad/anisrad,anismod,bet0,betinf)*nuovers
c$$$      endif
c
c mass at radius s = exp(lns):
c sum oveer dark component and all tracer components
c
      par2 = darkparam2
      Md11 = Mdarkatad11*mass(s/adark,darkmod,par2)
      Mtr11 = 0.d0
      do n = 1, ncomps
         par2 = tracerp2(n)
         if (debugg .ge. 4) then
            print *,' FNUSIGR2: n s atrac Mtrata11 model p2 = ',
     &          n, s, atrac(n),
     &          Mtratatr11(n), tracmod(n), par2
         endif
!        Mtr11 = Mtr11 + Mtratatr11(n)*mass(s/atrac(n),tracmod(n),par2)
! now follows procedure used in MAIN:
! need to add vectors lMtr and rfidtr in models2.i !
         Mtr11 = Mtr11 + 10.d0**(lMtr(n)-11.d0)
     &        *mass(s/atrac(n),tracmod(n),par2)
     &        /mass(rfidtr(n)/atrac(n),tracmod(n),par2)
      enddo  
      fnusigr2 = nuovers * K * (Mtr11 + Md11 + MBH11) 
      if (isnan(fnusigr2) .or. debugg .ge. 4) then
         print *,' FNUSIGR2: Mtr11 Md11 MBH11 = ', Mtr11, 
     &    Md11, MBH11
         print *,' s rad anisrad atracer bet0 betinf = ', s, rad, 
     &    anisrad, atracer, bet0, betinf
         print *,' FNUSIGR2: rad s atracer = ',
     &      sngl(rad), sngl(s), sngl(atracer)
         print *,' FNUSIGR2: beta0 betainf anisrad anismod = ',
     &      sngl(bet0), sngl(betinf), sngl(anisrad), anismod
         print *,' K(r,s) = ', K
         print *,' nuovers = ', nuovers
         print *,' adark = ', sngl(adark)
         print *,' M_d(a_d)11 = ', sngl(Mdarkatad11)
         par2 = darkparam2
         print *,' before call to MASS'
         print *,' Mdarktilde11 = ', sngl(mass(s/adark,darkmod,par2))
      endif 
      if (debugg .ge. 4) then
         print *,' FNUSIGR2:'
         if (s .lt. 1.00001d0*rad) then
            write(*,*) 'r        s       atr1     atr2    adark    K',
     &       '    nu    lMtr    lMd     lM    beta0  betainf  fnusigr2'
            write(115,*) 'r     atr(1)    atr(2)    adark    K',
     &          '    lMda   bet0    betinf'
            rm = 40000.d0
            write(115,*) rad, rm, atracer, adark, bet0, betinf, anisrad, 
     &          11.d0+dlog10(Mtratatr11(2)), 11.d0+dlog10(Mdarkatad11)
         endif
         write(*,90) rad, s, (atrac(n),n=1,ncomps), adark, K, 
     &     nuovers*s, 11.d0+dlog10(Mtr11), 11.d0+dlog10(Md11), 
     &     11.d0+dlog10(Mtr11+Md11+MBH11), bet0, betinf, fnusigr2
         write(114,*) 'r        s       atr1     atr2    adark    K',
     &    '    nu    lMtr    lMd     lM    beta0  betainf  fnusigr2'
         write(114,90) rad, s, (atrac(n),n=1,ncomps), adark, K, 
     &     nuovers*s, 11.d0+dlog10(Mtr11), 11.d0+dlog10(Md11), 
     &     11.d0+dlog10(Mtr11+Md11+MBH11), bet0, betinf, fnusigr2
 90      format(2(1g11.5,x),(2(1g11.5,x)), 9(1g11.5, x))
      endif
c
      return
      end
c
c
c
      double precision function fphi(lns)
c
c FPHI returns gravitational potential -Phi(r)/G, where Phi(r) is in units of (km/s)^2
c FPHI is not called for now
c
      implicit none
      real*8 lns
c
      real*8 s, mass, Ms11, Md11, par2
c
c common block
c
      include 'model2.i'
c
      s = dexp(lns)
c         
      par2 = tracerparam2
      Ms11 = Mtraceratatr11*mass(s/atracer,tracermod,par2)
      par2 = darkparam2
      Md11 = Mdarkatad11*mass(s/adark,darkmod,par2)
      fphi = (Ms11 + Md11 + MBH11) / s
c
      return
      end
c
c
c
      double precision function fldarkradius(lr)
c
c FLDARKRADIUS is a kernel to compute log(a_dark), even when c-M relation is given
c
      implicit none
      real*8 lr
c
      real*8 ad, mass, par2, lMvir, Mvir, c
c
c common block
c
      include 'model2.i'
      include 'debug.i'
c      
      ad = 10.d0**lr
c
      par2 = darkparam2
c
      if (aa0lclM .gt. 0.d0) then
c
c       M_vir (solar units) then c from incorporation of c-M relation
c
         lMvir = (11.d0 + dlog10(fourpideltarhocover3)
     &     + 3.d0*(aa0lclM+dlog10(ad)))/(1.d0-3.d0*aa1lclM)
         Mvir = 10.d0**lMvir
         c = 10.d0**(aa0lclM+aa1lclM*lMvir)
         rcube = mass(c,darkmod,par2)/(c*c*c)/fourpideltarhocover3
     &        *Mfid11
         if (debugg .ge. 4) then
            print *,' FLDARKRADIUS: log(a_d) Mfid11 rcube = ', 
     &           sngl(lr), sngl(Mfid11), sngl(rcube)
            print *,' Mvir c fourpi... = ', sngl(Mvir), sngl(c), 
     &           sngl(fourpideltarhocover3)
         end if
      endif
      fldarkradius = ad*ad*ad*mass(rfiddark/ad,darkmod,par2) - rcube
c
      return
      end
c
c
c
      double precision function flc(lc)
c
c FLC is a kernel to compute log(c)
c
      implicit none
      real*8 lc
c
      real*8 c, mass, par2
c
c common block
c
      include 'model2.i'
c      
      c = 10.d0**lc
c
      par2 = darkparam2
      flc = mass(c,darkmod,par2)/c**3.d0 - rhs
c
      return
      end
c
c
c
      double precision function flrvir(lrv)
c
c FLRV is a kernel to compute log(r_vir^tot)
c
      implicit none
      integer n
      real*8 lrv
c
      real*8 rv, mass, Mtr11, Md11, par2
c
c common block
c
      include 'params.i'
      include 'model2.i'
      include 'components.i'
      include 'debug.i'
c      
      rv = 10.d0**lrv
c
c      print *,' FLRVIR: rv = ', rv
      Mtr11 = 0.d0
      do n = 1, ncomps
         par2 = tracerp2(n)
!         Ms11 = Ms11 + Mtratatr11(n)*mass(rv/atrac(n),tracmod(n),par2)
! now follows procedure used in MAIN:
! need to add vectors lMtr and rfidtr in models2.i !
          Mtr11 = Mtr11 + 10.d0**(lMtr(n)-11.d0)
     &        *mass(rv/atrac(n),tracmod(n),par2)
     &        /mass(rfidtr(n)/atrac(n),tracmod(n),par2)
      enddo  
      par2 = darkparam2
      Md11 = Mdarkatad11*mass(rv/adark,darkmod,par2)
      flrvir = fourpideltarhocover3*rv**3.d0 - Mtr11 - Md11 - MBH11
      if (debugg .ge. 4) then
         print *,' FLRVIR: rhs = ', fourpideltarhocover3*rv**3.d0
         print *,' Md11 = ', Mdarkatad11*mass(rv/adark,darkmod,par2)
         print *,' MBH11 = ', MBH11
      end if
c
      return
      end
c
c
c
      double precision function Ksigmar(x,anismodel,beta0,betainf)
c
c KSIGMAR is a dimensionless kernel for the integration of nu sigma_r^2 from the Jeans equation
c
c arguments: r/a_anisotropy, anisotropy-model, beta_0, beta_inf
c notes: 
c   a_anisotropy is unity for anismodel = 'cst'
c
      implicit none
      real*8 x, beta0, betainf
      character*16 anismodel
c
      real*8 third
c
      if (x .lt. 0.d0) then
         print *,' Ksigmar: x = ', x, ' but requires x > 0!'
         stop
      else if (anismodel .eq. 'isotropic') then
         Ksigmar = 1.d0
      else if (anismodel .eq. 'constant') then   ! anisparam = beta
         Ksigmar = x**(2.d0*beta0)
      else if (anismodel .eq. 'OsipkovMerritt') then    ! Osipkov-Merritt
         Ksigmar = x*x+1.d0
      else if (anismodel .eq. 'MamonLokas') then    ! Mamon-Lokas
         Ksigmar = x+1.d0
      else if (anismodel .eq. 'DMS') then   ! Diemand-Moore-Stadel
         third = 1.d0/3.d0
         if (x .lt. 1.d0) then
            Ksigmar = dexp(6.d0*x**third)
         else
            Ksigmar = x*x*dexp(6.d0)
         endif
      else if (anismodel .eq. 'Tiret') then
         Ksigmar = x**(2.d0*beta0)*(x+1.d0)**(2.d0*(betainf-beta0))
      else
         print *,' KSIGMAR: cannot recognize anismodel = ', anismodel
         stop
      endif
c
      return
      end
c
c
c
      double precision function Ksigmarratio(x,y,anismodel,beta0,
     &  betainf)
c
c KSIGMARRATIO returns KSIGMAR(y)/KSIGMA(x), avoiding NaNs
c
      implicit none
      real*8 x, y, beta0, betainf
      character*16 anismodel
      integer ok
c
      ok = 0
      if (y .lt. x) then
         if (x .gt. 0.d0) then
            if (1.d0-y/x .lt.1.d-12) then
               ok = 1
            endif
         else
            if (x-y .lt. 1.d-14) then
               ok = 1
            endif
         endif
         if (ok .eq. 0) then
            print *,' KSIGMARATIO: y = ', y, ' < x = ', x
            stop
         endif
      endif
      if (anismodel .eq. 'isotropic') then
         Ksigmarratio = 1.d0
      else if (anismodel .eq. 'constant') then
         Ksigmarratio = (y/x)**(2.d0*beta0)
      else if (anismodel .eq. 'OsipkovMerritt') then
         Ksigmarratio = (y*y+1.d0)/(x*x+1.d0)
      else if (anismodel .eq. 'gOsipkovMerritt') then
         Ksigmarratio = (y/x)**(2.d0*beta0)*
     &     ((y*y+1.d0)/(x*x+1.d0))**(betainf-beta0)
      else if (anismodel .eq. 'MamonLokas') then
         Ksigmarratio = (y+1.d0)/(x+1.d0)
      else if (anismodel .eq. 'Tiret') then
         Ksigmarratio = (y/x)**(2.d0*beta0)*
     &     ((y+1.d0)/(x+1.d0))**(2.d0*(betainf-beta0))
c$$$      else if (anismodel .eq. 'Baes' .or. anismodel .eq. 'BvH') then
c$$$         ! delta parameter not yet implemented
c$$$         Ksigmarratio = (y/x)**(2.d0*beta0)*
c$$$     &     ((y**delta+1.d0)/(x**delta+1.d0))**
c$$$     &      (2.d0*(betainf-beta0)/delta)
      else
         print *,' KSIGMARRATIO cannot handle anismodel = ', anismodel
         stop
      endif
c
c if beta0 << 0, we can have underflows leading to Ksigmarratio = NaN
c
      if (isnan(Ksigmarratio)
     &  .and. x .gt. 0.d0 .and. y. gt. 0.d0
     &  .and. beta0 .le. 1.d0 .and. betainf .le. 1.d0) then
         Ksigmarratio = 0.d0
      endif
c
      return
      end
c
c
c
      double precision function arcsech(x)
      implicit none
      real*8 x
c
      if (x .le. 0.d0) then
         print *,' ARCSECH: x =', x, ' must be positive!'
         stop
      endif
c
      arcsech = dlog(dsqrt(1.d0/(x*x)-1.d0)+1.d0/x)
c
      return
      end
c
c
c
      double precision function arccosh(x)
      implicit none
      real*8 x
c
      if (x .lt. 1.d0) then
         print *,' ARCCOSH: x =', x, ' must be >= 1!'
         stop
      endif
c
      arccosh = dlog(x+dsqrt(x*x-1.d0))
c
      return
      end
c
c
c
      double precision function asinh(x)
c
c ASINH returns sinh^-1 (x)
c
      implicit none
      real*8 x
c
      asinh = dlog(x+dsqrt(x*x+1.d0))
c
      return
      end
c
c
c
      double precision function arcsec(x)
      implicit none
      real*8 x
c
      if (x .lt. 1.d0) then
         print *,' ARCSEC: x =', x, ' must be >= 1!'
         stop
      endif
c
      arcsec = dacos(1.d0/x)
c
      return
      end
c
c
c
      double precision function aco(x)
c
c ACO is a generalized arccos/arccosh
c
      implicit none
      real*8 x
c
      real*8 arccosh
c
      if (x .lt. 1.d0) then
         aco = dacos(x)
      else if (x .gt. 1.d0) then
         aco = arccosh(x)
      else if (x .eq. 1.d0) then
         aco = 0.d0
      else
         print *,' ACO: cannot handle x=', x
         stop
      endif
c
      return
      end
c
c
c
      double precision function ase(x)
c
c ASE is a generalized arcsec/arcsech
c
      implicit none
      real*8 x
      real*8 arcsech, arcsec
c
      if (x .lt. 1.d0) then
         ase = arcsech(x)
      else if (x .gt. 1.d0) then
         ase = arcsec(x)
      else
         print *,' ASE: cannot handle x=1'
         stop
      endif
c
      return
      end
c
c
c
      logical function isnan(x)
      implicit none
      real*8 x
      isnan = .true.
c
      if (x .ge. 0.d0) then
         isnan = .false.
      elseif (x .le. 0.d0) then
         isnan = .false.
      endif
c
      return
      end
c
c
c
      logical function isinf(x)
      implicit none
      real*8 x
c
      if (abs(x) > huge(x)) then
         isinf = .true.
      else
         isinf = .false.
      endif
      end
c
c
c
      double precision function Ailop(X)
c
c Ailop is the A function in the universal distribution of interlopers in projected phase space
c i.e. the normalization of the gaussian velocity term, in units of N_vir/(r_vir^2 v_vir)
c
c arg: R/r_vir
c source: Mamon, Biviano & Murante 2010
c
      implicit none
      real*8 X
c
      real*8 X2, X4
c
      X2 = X*X
      X4 = X2*X2
      Ailop = 10.d0**(-1.061d0 + 0.364d0*X2 - 0.580d0*X4 +0.533d0*X4*X2)
c
      return
      end
c
c
c
      double precision function sigmailop(X)
c
c sigmailop is the sigmai function in the universal distribution of interlopers in PPS 
c i.e. the dispersion of the gaussian velocity term in units of v_vir
c
c arg: R/r_vir
c source: Mamon, Biviano & Murante 2010
c
      implicit none
      real*8 X
c
      sigmailop = 0.612d0 - 0.0653d0*X*X
c
      return
      end
c
c
c
      double precision function fNilop(X)
c
c Nilopint is the integrand used to derive Nilop/(2pi) in units of N(r_v)
c
      implicit none
      real*8 X
      real*8 erf, silop, sigmailop, Ailop
      include 'ilop.i'
c
      silop = sigmailop(X)
      fNilop = X*dsqrt(dacos(0.d0))*Ailop(X)*silop
     & *erf(kappa/dsqrt(2.d0)/silop) + kappa*Bilop
C$$$      write(*,90) X, Ailop(X), silop, kappa, Bilop, fNilop
C$$$ 90     format(6(g13.5))
c
      return
      end
c
c
c
      double precision function gilophat(X,Y,B)
c
c gilophat is the dimensionless interloper PPS density in virial units
c
c args: R/r_vir, |v_LOS|/v_vir, B
c
      implicit none
      real*8 X, Y, B
      real*8 Ailop, sigmailop
      gilophat = Ailop(X)*dexp(-0.5d0*(Y/sigmailop(X))**2.d0) + B
c
      return
      end
c
c
c
      double precision function Iilopapx(X)
c
c Iilopapx is the approximation to integer_0^X X A(X) sigma_i(X) dX
c see MAMPOSST/CODE_GAM/denomqilop.nb
c
      implicit none
      real*8 X
c
      integer i
      real*8 a(4), aa, bb
      a(1) = 0.026590d0
      a(2) = 0.0097246d0
      a(3) =  -0.0095165d0
      a(4) =  0.0020680d0
      aa = 1.29346d0
      bb = 7.25428d0
c      
      Iilopapx = 0.d0
      do i = 1, 4
         Iilopapx = Iilopapx + a(i)*X**(2.d0*dfloat(i))
      enddo
      Iilopapx = Iilopapx*dexp((X/aa)**bb)
c
      return
      end
c
c
c
      double precision function Eofz(Omegam,Omegal,z)
c
c Eofz is the dimensionless Hubble parameter
c Omegam and Omegal are Omega_matter and _mega_lambda at z=0
c
      implicit none
      real*8 Omegam, Omegal, z
      real*8 z1
c
      z1 = 1.d0+z
      Eofz = sqrt(Omegam*z1*z1*z1 + (1.d0-Omegam-Omegal)*z1*z1 
     & + Omegal)
c
      return
      end
c
c
c
      double precision function gaussian(x,mean,sig)
c
c GAUSSIAN returns a Gaussian
c
      implicit none
      real*8 x, mean , sig
      include 'csts.i'
      gaussian = 1.d0/(sqrttwopi*sig)*dexp(-0.5d0*((x-mean)/sig)**2.d0)
c
      return
      end
c
c
c
      double precision function dmod2d(mu)
c
c DMOD2D returns the distance in Mpc given the distance modulus
c
      implicit none
      real*8 mu
      dmod2d =  10.d0**(0.2d0*mu)/1.d5
c
      return
      end
c
c
c
        SUBROUTINE HYGFX(A,B,C,X,HF)
C
C       ====================================================
C       Purpose: Compute hypergeometric function F(a,b,c,x)
C       Input :  a --- Parameter
C                b --- Parameter
C                c --- Parameter, c <> 0,-1,-2,...
C                x --- Argument   ( x < 1 )
c Gary Mamon: a b and c must be specified as separate variables
c   in the calling function!
C       Output:  HF --- F(a,b,c,x)
C       Routines called:
C            (1) GAMMA for computing gamma function
C            (2) PSI for computing psi function
C       ====================================================
C
        IMPLICIT DOUBLE PRECISION (A-H,O-Z)
        LOGICAL L0,L1,L2,L3,L4,L5
        PI=3.141592653589793D0
        EL=.5772156649015329D0
        L0=C.EQ.INT(C).AND.C.LT.0.0
        L1=1.0D0-X.LT.1.0D-15.AND.C-A-B.LE.0.0
        L2=A.EQ.INT(A).AND.A.LT.0.0
        L3=B.EQ.INT(B).AND.B.LT.0.0
        L4=C-A.EQ.INT(C-A).AND.C-A.LE.0.0
        L5=C-B.EQ.INT(C-B).AND.C-B.LE.0.0
        IF (L0.OR.L1) THEN
           WRITE(*,*)'The hypergeometric series is divergent'
           RETURN
        ENDIF
        EPS=1.0D-15
        IF (X.GT.0.95) EPS=1.0D-8
        IF (X.EQ.0.0.OR.A.EQ.0.0.OR.B.EQ.0.0) THEN
           HF=1.0D0
           RETURN
        ELSE IF (1.0D0-X.EQ.EPS.AND.C-A-B.GT.0.0) THEN
           CALL GAMMA(C,GC)
           CALL GAMMA(C-A-B,GCAB)
           CALL GAMMA(C-A,GCA)
           CALL GAMMA(C-B,GCB)
           HF=GC*GCAB/(GCA*GCB)
           RETURN
        ELSE IF (1.0D0+X.LE.EPS.AND.DABS(C-A+B-1.0).LE.EPS) THEN
           G0=DSQRT(PI)*2.0D0**(-A)
           CALL GAMMA(C,G1)
           CALL GAMMA(1.0D0+A/2.0-B,G2)
           CALL GAMMA(0.5D0+0.5*A,G3)
           HF=G0*G1/(G2*G3)
           RETURN
        ELSE IF (L2.OR.L3) THEN
           IF (L2) NM=INT(ABS(A))
           IF (L3) NM=INT(ABS(B))
           HF=1.0D0
           R=1.0D0
           DO 10 K=1,NM
              R=R*(A+K-1.0D0)*(B+K-1.0D0)/(K*(C+K-1.0D0))*X
10            HF=HF+R
           RETURN
        ELSE IF (L4.OR.L5) THEN
           IF (L4) NM=INT(ABS(C-A))
           IF (L5) NM=INT(ABS(C-B))
           HF=1.0D0
           R=1.0D0
           DO 15 K=1,NM
              R=R*(C-A+K-1.0D0)*(C-B+K-1.0D0)/(K*(C+K-1.0D0))*X
15            HF=HF+R
           HF=(1.0D0-X)**(C-A-B)*HF
           RETURN
        ENDIF
        AA=A
        BB=B
        X1=X
        IF (X.LT.0.0D0) THEN
           X=X/(X-1.0D0)
           IF (C.GT.A.AND.B.LT.A.AND.B.GT.0.0) THEN
              A=BB
              B=AA
           ENDIF
           B=C-B
        ENDIF
        IF (X.GE.0.75D0) THEN
           GM=0.0D0
           IF (DABS(C-A-B-INT(C-A-B)).LT.1.0D-15) THEN
              M=INT(C-A-B)
              CALL GAMMA(A,GA)
              CALL GAMMA(B,GB)
              CALL GAMMA(C,GC)
              CALL GAMMA(A+M,GAM)
              CALL GAMMA(B+M,GBM)
              CALL PSI(A,PA)
              CALL PSI(B,PB)
              IF (M.NE.0) GM=1.0D0
              DO 30 J=1,ABS(M)-1
30               GM=GM*J
              RM=1.0D0
              DO 35 J=1,ABS(M)
35               RM=RM*J
              F0=1.0D0
              R0=1.0D0
              R1=1.0D0
              SP0=0.D0
              SP=0.0D0
              IF (M.GE.0) THEN
                 C0=GM*GC/(GAM*GBM)
                 C1=-GC*(X-1.0D0)**M/(GA*GB*RM)
                 DO 40 K=1,M-1
                    R0=R0*(A+K-1.0D0)*(B+K-1.0)/(K*(K-M))*(1.0-X)
40                  F0=F0+R0
                 DO 45 K=1,M
45                  SP0=SP0+1.0D0/(A+K-1.0)+1.0/(B+K-1.0)-1.0/K
                 F1=PA+PB+SP0+2.0D0*EL+DLOG(1.0D0-X)
                 DO 55 K=1,250
                    SP=SP+(1.0D0-A)/(K*(A+K-1.0))+(1.0-B)/(K*(B+K-1.0))
                    SM=0.0D0
                    DO 50 J=1,M
50                     SM=SM+(1.0D0-A)/((J+K)*(A+J+K-1.0))+1.0/
     &                    (B+J+K-1.0)
                    RP=PA+PB+2.0D0*EL+SP+SM+DLOG(1.0D0-X)
                    R1=R1*(A+M+K-1.0D0)*(B+M+K-1.0)/(K*(M+K))*(1.0-X)
                    F1=F1+R1*RP
                    IF (DABS(F1-HW).LT.DABS(F1)*EPS) GO TO 60
55                  HW=F1
60               HF=F0*C0+F1*C1
              ELSE IF (M.LT.0) THEN
                 M=-M
                 C0=GM*GC/(GA*GB*(1.0D0-X)**M)
                 C1=-(-1)**M*GC/(GAM*GBM*RM)
                 DO 65 K=1,M-1
                    R0=R0*(A-M+K-1.0D0)*(B-M+K-1.0)/(K*(K-M))*(1.0-X)
65                  F0=F0+R0
                 DO 70 K=1,M
70                  SP0=SP0+1.0D0/K
                 F1=PA+PB-SP0+2.0D0*EL+DLOG(1.0D0-X)
                 DO 80 K=1,250
                    SP=SP+(1.0D0-A)/(K*(A+K-1.0))+(1.0-B)/(K*(B+K-1.0))
                    SM=0.0D0
                    DO 75 J=1,M
75                     SM=SM+1.0D0/(J+K)
                    RP=PA+PB+2.0D0*EL+SP-SM+DLOG(1.0D0-X)
                    R1=R1*(A+K-1.0D0)*(B+K-1.0)/(K*(M+K))*(1.0-X)
                    F1=F1+R1*RP
                    IF (DABS(F1-HW).LT.DABS(F1)*EPS) GO TO 85
80                  HW=F1
85               HF=F0*C0+F1*C1
              ENDIF
           ELSE
              CALL GAMMA(A,GA)
              CALL GAMMA(B,GB)
              CALL GAMMA(C,GC)
              CALL GAMMA(C-A,GCA)
              CALL GAMMA(C-B,GCB)
              CALL GAMMA(C-A-B,GCAB)
              CALL GAMMA(A+B-C,GABC)
              C0=GC*GCAB/(GCA*GCB)
              C1=GC*GABC/(GA*GB)*(1.0D0-X)**(C-A-B)
              HF=0.0D0
              R0=C0
              R1=C1
              DO 90 K=1,250
                 R0=R0*(A+K-1.0D0)*(B+K-1.0)/(K*(A+B-C+K))*(1.0-X)
                 R1=R1*(C-A+K-1.0D0)*(C-B+K-1.0)/(K*(C-A-B+K))
     &              *(1.0-X)
                 HF=HF+R0+R1
                 IF (DABS(HF-HW).LT.DABS(HF)*EPS) GO TO 95
90               HW=HF
95            HF=HF+C0+C1
           ENDIF
        ELSE
           A0=1.0D0
           IF (C.GT.A.AND.C.LT.2.0D0*A.AND.
     &         C.GT.B.AND.C.LT.2.0D0*B) THEN
              A0=(1.0D0-X)**(C-A-B)
              A=C-A
              B=C-B
           ENDIF
           HF=1.0D0
           R=1.0D0
           DO 100 K=1,250
              R=R*(A+K-1.0D0)*(B+K-1.0D0)/(K*(C+K-1.0D0))*X
              HF=HF+R
              IF (DABS(HF-HW).LE.DABS(HF)*EPS) GO TO 105
100           HW=HF
105        HF=A0*HF
        ENDIF
        IF (X1.LT.0.0D0) THEN
           X=X1
           C0=1.0D0/(1.0D0-X)**AA
           HF=C0*HF
        ENDIF
        A=AA
        B=BB
        IF (K.GT.120) WRITE(*,115)
115     FORMAT(1X,'Warning! You should check the accuracy')
        RETURN
        END


        SUBROUTINE GAMMA(X,GA)
C
C       ==================================================
C       Purpose: Compute gamma function �(x)
C       Input :  x  --- Argument of �(x)
C                       ( x is not equal to 0,-1,-2,���)
C       Output:  GA --- �(x)
C       ==================================================
C
        IMPLICIT DOUBLE PRECISION (A-H,O-Z)
        DIMENSION G(26)
        PI=3.141592653589793D0
        IF (X.EQ.INT(X)) THEN
           IF (X.GT.0.0D0) THEN
              GA=1.0D0
              M1=X-1
              DO 10 K=2,M1
10               GA=GA*K
           ELSE
              GA=1.0D+300
           ENDIF
        ELSE
           IF (DABS(X).GT.1.0D0) THEN
              Z=DABS(X)
              M=INT(Z)
              R=1.0D0
              DO 15 K=1,M
15               R=R*(Z-K)
              Z=Z-M
           ELSE
              Z=X
           ENDIF
           DATA G/1.0D0,0.5772156649015329D0,
     &          -0.6558780715202538D0, -0.420026350340952D-1,
     &          0.1665386113822915D0,-.421977345555443D-1,
     &          -.96219715278770D-2, .72189432466630D-2,
     &          -.11651675918591D-2, -.2152416741149D-3,
     &          .1280502823882D-3, -.201348547807D-4,
     &          -.12504934821D-5, .11330272320D-5,
     &          -.2056338417D-6, .61160950D-8,
     &          .50020075D-8, -.11812746D-8,
     &          .1043427D-9, .77823D-11,
     &          -.36968D-11, .51D-12,
     &          -.206D-13, -.54D-14, .14D-14, .1D-15/
           GR=G(26)
           DO 20 K=25,1,-1
20            GR=GR*Z+G(K)
           GA=1.0D0/(GR*Z)
           IF (DABS(X).GT.1.0D0) THEN
              GA=GA*R
              IF (X.LT.0.0D0) GA=-PI/(X*GA*DSIN(PI*X))
           ENDIF
        ENDIF
        RETURN
        END


        SUBROUTINE PSI(X,PS)
C
C       ======================================
C       Purpose: Compute Psi function
C       Input :  x  --- Argument of psi(x)
C       Output:  PS --- psi(x)
C       ======================================
C
        IMPLICIT DOUBLE PRECISION (A-H,O-Z)
        XA=DABS(X)
        PI=3.141592653589793D0
        EL=.5772156649015329D0
        S=0.0D0
        IF (X.EQ.INT(X).AND.X.LE.0.0) THEN
           PS=1.0D+300
           RETURN
        ELSE IF (XA.EQ.INT(XA)) THEN
           N=XA
           DO 10 K=1 ,N-1
10            S=S+1.0D0/K
           PS=-EL+S
        ELSE IF (XA+.5.EQ.INT(XA+.5)) THEN
           N=XA-.5
           DO 20 K=1,N
20            S=S+1.0/(2.0D0*K-1.0D0)
           PS=-EL+2.0D0*S-1.386294361119891D0
        ELSE
           IF (XA.LT.10.0) THEN
              N=10-INT(XA)
              DO 30 K=0,N-1
30               S=S+1.0D0/(XA+K)
              XA=XA+N
           ENDIF
           X2=1.0D0/(XA*XA)
           A1=-.8333333333333D-01
           A2=.83333333333333333D-02
           A3=-.39682539682539683D-02
           A4=.41666666666666667D-02
           A5=-.75757575757575758D-02
           A6=.21092796092796093D-01
           A7=-.83333333333333333D-01
           A8=.4432598039215686D0
           PS=DLOG(XA)-.5D0/XA+X2*(((((((A8*X2+A7)*X2+
     &        A6)*X2+A5)*X2+A4)*X2+A3)*X2+A2)*X2+A1)
           PS=PS-S
        ENDIF
        IF (X.LT.0.0) PS=PS-PI*DCOS(PI*X)/DSIN(PI*X)-1.0D0/X
        RETURN
        END
c
c
c
      integer function sgn(x)
c
c SGN returns integer sign of x
c
      implicit none
      real*8 x
      if (x .gt. 0.d0) then
         sgn = 1
      else if (x .lt. 0.d0) then
         sgn = -1
      else if (x .eq. 0.d0) then
         sgn = 0
      else
         print *,' cannot find sign of x = ', x
         stop
      endif
c
      return
c
      end
c
c
c
      subroutine qagserr(ier)
      implicit none
      integer ier
c     
      if (ier .eq. 1) then
         print *,' max num of interations in DQAGS'
      else if (ier .eq. 2) then
         print *,' roundoff error in DQAGS'
      else if (ier .eq. 3) then
         print *,' bad integrand behavior DQAGS'
      else if (ier .eq. 4) then
         print *,' no convergence in DQAGS'
      else if (ier .eq. 4) then
         print *,' divergent or slow convergence in DQAGS'
      else if (ier .eq. 6) then
         print *,' invalid input to DQAGS'
      end if
c     
      return
      end
c
c
c
      subroutine gaus8err(ier)
      implicit none
      integer ier
c     
      if (ier .eq. -1) then
         print *, ' integration limits are to close for DGAUS8'
      else if (ier .eq. 2) then
         print *, ' cannot meet tolerance in DGAUS8'
      end if
c     
      return
      end
      
