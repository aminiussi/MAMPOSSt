module CalcLike
  use CMB_Cls
  use cmbtypes
  use cmbdata
  use mpk
  use Random
  use settings
  use ParamDef
  use snovae
  use WeakLen
  use Lya

  implicit none

  logical :: Use_Age_Tophat_Prior = .true.
  logical :: Use_CMB = .true.
  logical :: Use_BBN = .false.
  logical :: Use_Clusters = .false.

  integer :: H0_min = 40, H0_max = 100
  real :: Omk_min = -0.3, Omk_max = 0.3
  real :: Use_min_zre = 0
  integer :: Age_min = 10, Age_max = 20
  real :: Temperature  = 1

contains

  function GenericLikelihoodFunction(Params) 
    type(ParamSet)  Params 
    real :: GenericLikelihoodFunction

    ! extra declarations by GAM

    integer, parameter :: MAXOBJ = 10000, MAXRSPL = 100
    integer :: ndata, nfilter
    real*8, dimension(MAXOBJ) :: rr, vz, avz, evz, wt
    real*8, dimension(MAXOBJ) :: rrfilter, avzfilter, evzfilter
    real*8, dimension(MAXOBJ) :: mufilter, emufilter
    character (len=16), dimension(MAXOBJ) :: compfilter

    integer :: n, ndum, iread, debug, nn, np, npdum, nc, ncdata, found, ndatafilter, nn2(MAXCOMP,MAXOBJ)
    character(len=20) :: str
    logical, save :: past_read_data = .false.
    real*8 :: mamposstlnlik, rrdum, vzdum, rrmin0, rrmax0, avzmax0
    integer :: dscalefg, rrflag, avzflag
    real*8 :: HUGE=1.d37, SMALL=1.d-8, TINY=1.d-16, mdark
    real*8 :: andataincomp
    integer :: i, j, k, imax, ier, idx(MAXOBJ), ndataincomp
    integer :: i1, i2, ilast, jmax(MAXCOMP)
    real*8, dimension(MAXOBJ) :: rrdata, vzdata, avzdata, evzdata
    real*8, dimension(MAXOBJ) :: mudata, emudata
    real*8 :: rr0(MAXRSPL), wt0, lwt0(MAXRSPL)
    real*8 :: lwt(MAXOBJ), rr2
    real*8 :: sumvz, sumvz2, meanvz, meanvz2, sigmavz(MAXCOMP)
    real*8, dimension(MAXOBJ) :: rrtmp, avztmp
    real*8 :: yy1, yy2, ypp(MAXOBJ)
    character (LEN=16) :: compdata(MAXOBJ), compd, cdata
    character (LEN=Ini_max_string_len) :: file

    ! end of extra definitions by GAM

    ! block by GAM
    !
    ! read data file
    ! 2 header lines
    ! R [kpc] v_z [km/s] error(v_z) [km/s] (not more than MAXOBJ lines)

    if (.not. past_read_data) then
       file = trim(data_directory)//'/'//data_file
       if (debugMP .ge. 1) then
          print *,' in CALCLIKE, reading data file ', trim(file)
       end if
       open(3,file=trim(file))
       ! skip header lines
       do i = 1, header_lines
          read (3,*) str
       end do

       ! read data

       do i = 1, MAXOBJ
          read(3,*,END=392) rrdata(i), vzdata(i), evzdata(i), compdata(i), mudata(i), emudata(i)
          avzdata(i) = dabs(vzdata(i))
       enddo
392    continue
       close (3)
       ndata = i-1

       if (ndata .lt. 2) then
          print *,' number of tracers in data file = ', ndata, ' is too small for MAMPOSSt!'
          stop
       else if (debugMP .ge. 1) then
          print *,' found ', ndata, ' rows of data'
       endif

       ! check that components in data file are all recognized

       do ndum = 1, ndata
          compd = compdata(ndum)
          cdata = compd(index(compd,'_')+1:)
          read(compd(1:index(compd,'_')-1),*) ncdata
          found = 0
          do n = 1, ncomp
             if (trim(comp(n)) .eq. trim(cdata) .and. n .eq. ncdata) then
                found = 1
                exit
             endif
          enddo
          if (found .eq. 0) then
             print *,' CALCLIKE: cannot recognize component-column = AAA', trim(cdata), 'ZZZ'
             print *,' in row ', ndum, ' of data file ', trim(file)
             print *,' with ncdata = ', ncdata
             print *,' from sncdata = AAA', compd(1:index(compd,'_')-1), 'ZZZ'
             print *,' while components are:'
             do n = 1, ncomp
                print *, n, 'AAA',trim(comp(n)),'ZZZ'
             enddo
             print *,' stopping because component not found'
             stop
          endif
       enddo

       ! determine from data: min and max R and max |v_z|

       nn = 0
       do n = 1, ncomp
          if (debugMP .ge. 1) print *,' processing component ', n, ' ', trim(comp(n)), ' ...'
          sumvz = 0.d0
          sumvz2 = 0.d0
          ! adjust rrminallow, etc.

          if (rrminallow(n) .lt. 0.d0) then
             rrminallow(n) = 0.d0
          endif
          if (rrmaxallow(n) .le. 0.d0) then
             rrmaxallow(n) = HUGE
          endif
          if (avzmaxallow(n) .le. 0.d0) then
             avzmaxallow(n) = HUGE
          endif
          if (debugMP .ge. 1) then
             print *,' Rminallow Rmaxallow avzmaxallow = ', &
                  rrminallow(n), rrmaxallow(n), avzmaxallow(n)
          end if 

          ! filter data for component
          j = 0
          do i = 1, ndata
             compd = compdata(i)
             cdata = compd(index(compd,'_')+1:)
             read (compd(1:index(compd,'_')-1),*) ncdata
             if (cdata .eq. comp(n) .and. ncdata .eq. n) then
                if (rrdata(i) .ge. rrminallow(n) .and. rrdata(i) .le. rrmaxallow(n) .and. avzdata(i) .le. avzmaxallow(n)) then
                   j = j+1
                   rrtmp(j) = rrdata(i)
                   avztmp(j) = avzdata(i)
                   sumvz = sumvz + vzdata(i)
                   sumvz2 = sumvz2 + avztmp(j)*avztmp(j)
                   nn = nn + 1
                   nn2(n,j) = nn
                   rrfilter(nn) = rrdata(i)
                   avzfilter(nn) = avzdata(i)
                   evzfilter(nn) = evzdata(i)
                   compfilter(nn) = compdata(i)
                   wt(nn) = 1.d0
                   mufilter(nn) = mudata(i)
                   emufilter(nn) = emudata(i)
                else if (debugMP .ge. 2) then
                   print *,' rejecting n R vz = ', i, rrdata(i), vzdata(i)
                endif
             endif
          enddo  ! end of data loop
          ndataincomp = j
          andataincomp = dfloat(ndataincomp)
          if (debugMP .ge. 2) then
             print *,' number of points for component ', trim(comp(n)), ' = ', ndataincomp
          end if
          if (ndataincomp .le. 1) then
             print *,' CALCLIKE: only ', ndataincomp, ' tracers found for component ', n, ' ', trim(comp(n))
          endif
          if (ndataincomp .le. 1) then
             sigmavz(n) = -1.d0
             rrmindata(n) = -1.d0
             rrmaxdata(n) = -1.d0
             avzmaxdata(n) = -1.d0
             cycle
          else
             sigmavz(n) = dsqrt(andataincomp/(andataincomp-1.d0)*(sumvz2/andataincomp-(sumvz/andataincomp)**2.d0))
             rrmindata(n) = minval(rrtmp)
             rrmaxdata(n) = maxval(rrtmp)
             avzmaxdata(n) = maxval(avztmp)
          endif

          ! adjust rrminallow, etc.

          if (rrmaxallow(n) .ge. HUGE*(1.d0-TINY)) then
             rrmaxallow(n) = rrmaxdata(n)*(1.d0+TINY)
          endif
          if (avzmaxallow(n) .ge. HUGE*(1.d0-TINY)) then
             avzmaxallow(n) = avzmaxdata(n)*(1.d0+TINY)
          endif
          if (rrminallow(n) .lt. 0.d0) then
             rrminallow(n) = rrmindata(n)*(1.d0-TINY)
          endif
          if (rrmaxallow(n) .le. 0.d0) then
             rrmaxallow(n) = rrmaxdata(n)*(1.d0+TINY)
          endif
          if (avzmaxallow(n) .le. 0.d0) then
             avzmaxallow(n) = avzmaxdata(n)*(1.d0+TINY)
          endif
          if (debugMP .ge. 2) then
             print *,' rrmindata rrmaxdata = ', rrmindata(n), rrmaxdata(n)
             print *, ' avzmaxdata = ', avzmaxdata(n)
             print *,' rrminallow rrmaxallow = ', rrminallow(n), rrmaxallow(n)
             print *, ' avzmaxallow = ', avzmaxallow(n)
          end if

          ! avoids singularity at R=0 for sigma_r(r): start at 10% of 2nd lowest R value

          if (rrmindata(n) .lt. SMALL .and. rrminallow(n) .lt. SMALL) then
             rrmindata(n) = 0.1d0*rrtmp(idx(2))
             rrminallow(n) = rrmindata(n)
          endif

          ! weights

          if (completenessofRfile(n) .ne. 'none' .or. weightofRfile(n) .ne. 'none') then
             if (debugMP .ge. 1) print *,' n completenessfile = ', n, completenessofRfile(n)
             if (completenessofRfile(n) .ne. 'none') then
                open (3,file=trim(data_directory)//'/'//completenessofRfile(n))
             else
                open(3,file=trim(data_directory)//'/'//weightofRfile(n))
             endif
             ! skip 2 header lines
             read (3,*) str
             read (3,*) str
             do i = 1, MAXOBJ
                read(3,*,END=393) rr0(i), wt0
                if (wt0 .le. 0.d0) wt0 = SMALL
                if (completenessofRfile(n) .ne. 'none') then
                   ! inverse weights! w = 1/C
                   lwt0(i) = -1.d0*dlog10(wt0)
                else
                   lwt0(i) = dlog10(wt0)                     
                endif
             enddo
393          continue

             ! fit cubic spline to log w vs R

             imax = i-1
             call SPLINE_CUBIC_SET(imax,rr0,lwt0,2,0.d0,2,0.d0,ypp)
             do j = 1, ndataincomp
                nn = nn2(n,j)
                call SPLINE_CUBIC_VAL(imax,rr0,lwt0,ypp,rrtmp(j),lwt(j),yy1,yy2)
                wt(nn) = 10.d0**lwt(j)
             enddo
          endif
          if (weightflag .eq. 1) then
             do j = 1, ndataincomp
                nn = nn2(n,j)
                wt(nn) = dexp(-avzfilter(nn)**2.d0/(2.d0*sigmavz(n)**2.d0))
             enddo
          endif
       enddo ! end of component loop
       nfilter = nn
       past_read_data = .true.
    endif  ! end of 1st read block

    ! convert parameters to MAMPOSSt log-likeihood function
    
    norm = dble(Params%P(1))
    darkscale = dble(Params%P(2))
    darkpar2 = dble(Params%P(3))
    ltracermasstot = dble(Params%P(4))
    do n = 1, ncomp
       ltracerradius(n) = dble(Params%P(        4+n))
       ltracermass(n)   = dble(Params%P(  ncomp+4+n))
       tracerpar2(n)    = dble(Params%P(2*ncomp+4+n))
       fractracer(n)    = dble(Params%P(3*ncomp+4+n))
       lanis0(n)        = dble(Params%P(4*ncomp+4+n))
       lanisinf(n)      = dble(Params%P(5*ncomp+4+n))
       lanisradius(n)   = dble(Params%P(6*ncomp+4+n))
    end do
    np = 7*ncomp + 4
    lbhmass = dble(Params%P(np+1))
    lBilop  = dble(Params%P(np+2))
    
    if (debugMP .ge. 2) then
       print *,' CALCLIKE ...'
       do npdum = 1, np + 3*ncomp+1
          print *,' np Param = ', npdum, Params%P(npdum)
       enddo
       do n = 1, ncomp
          print *,' nc ltracerradius = ', n, ltracerradius(n)
          print *,' nc ltracermass = ', n, ltracermass(n)
          print *,' nc tracerpar2 = ', n, tracerpar2(n)
          print *,' nc fractracer = ', n, fractracer(n)
       enddo
       print *,' ltracermasstot = ', ltracermasstot
       print *,' norm = ', norm
       print *,' darkscale = ', darkscale
       print *,' darkpar2 = ', darkpar2
       do n = 1, ncomp
          print *,' nc lanis0 = ', n, lanis0(n)
          print *,' nc lanisinf = ', n, lanisinf(n)
          print *,' nc lanisradius = ', n, lanisradius(n)
       enddo
       print *,' lbhmass = ', lbhmass
       print *,' lBilop = ', lBilop

       print *,' CALCLIKE: 1st 10 data points:'
       do n = 1, 10
          print *,' R v comp = ', rrfilter(n), avzfilter(n), compfilter(n)
       enddo
       print *,' Now about to enter MAMPOSSTLNLIK: ncomp debugMP = ', ncomp, debugmP, ' ...'
       print *,' and nfilter tracermodel anismodel = ', nfilter, tracermodel, anismodel, ' ...'
       print *,' and lanis0 lanisinf lanisradius = ', lanis0, lanisinf, lanisradius, ' ...'
    endif
    GenericLikelihoodFunction = real(mamposstlnlik(rrfilter,avzfilter,evzfilter, &
         compfilter,mufilter,emufilter,nfilter, &
         rrmindata,rrmaxdata,rrminallow,rrmaxallow,avzmaxdata,avzmaxallow,mu0, &
         tracermodel,ltracerradius,meanltracerradius,sigltracerradius,tracerpar2, &
         ltracermass,ltracermasstot,fractracer,rfidtracer, &
         anismodel,anisflag,lanis0,lanisinf,lanisradius,ncomp, &
         darkmodel,norm,darknormflag,darkscale,darkscaleflag, &
         darktotflag,darkpar2, &
         lbhmass,v3dmodel,&
         DeltaNL,hubble,Omegam,z,&
         rmax,MfLflag,TLMflag,TALflag,a0lclM,a1lclM,siglc,splitpflag,&
         wt,distflag,lBilop,ilopflag,&
         debugMP))

    if (isnan(GenericLikelihoodFunction)) GenericLikelihoodFunction = LogZero

  end function GenericLikelihoodFunction

  
  function GetLogPrior(CMB, Info) !Get -Ln(Prior)
    real GetLogPrior
    real Age
    Type (CMBParams) CMB
    Type(ParamSetInfo) Info

    GetLogPrior = logZero
 
    if (.not. generic_mcmc) then
     if (CMB%H0 < H0_min .or. CMB%H0 > H0_max) return
     if (CMB%omk < Omk_min .or. CMB%omk > Omk_max .or. CMB%Omv < 0) return
     if (CMB%zre < Use_min_zre) return

     Age = GetAge(CMB, Info)
      !This sets up parameters in CAMB, so do not delete unless you are careful!
 
     if (Use_Age_Tophat_Prior .and. (Age < Age_min .or. Age > Age_max) .or. Age < 0) return
    
    end if
    GetLogPrior = 0
 
  end function GetLogPrior

  function GetLogLike(Params) !Get -Ln(Likelihood)
    type(ParamSet)  Params 
    Type (CMBParams) CMB
    real GetLogLike
    real dum(1,1)
 
    if (any(Params%P > Scales%PMax) .or. any(Params%P < Scales%PMin)) then
       GetLogLike = logZero
        return
    end if

    if (generic_mcmc) then
        GetLogLike = GenericLikelihoodFunction(Params) 
        if (GetLogLike /= LogZero) GetLogLike = GetLogLike/Temperature

    else

     call ParamsToCMBParams(Params%P,CMB)

     GetLogLike = GetLogLikePost(CMB, Params%Info,dum,.false.)
    end if 
   end function GetLogLike

    
  function GetLogLikePost(CMB, Info, inCls, HasCls) 
    use cambmain, only: initvars
    use Camb, only: CAMBParams_Set 
    type(CAMBParams)  P
    real GetLogLikePost
    Type (CMBParams) CMB
    Type(ParamSetInfo) Info
    real, intent(in):: inCls(:,:)
    logical, intent(in) :: HasCls
    real acl(lmax,num_cls_tot)
    integer error

    if (generic_mcmc) stop 'GetLogLikePost: not supported for generic'

    GetLogLikePost  = GetLogPrior(CMB, Info)
    if ( GetLogLikePost >= logZero) then
       GetLogLikePost = logZero
       
    else 
       GetLogLikePost = GetLogLikePost + sum(CMB%nuisance(1:nuisance_params_used)**2)/2
          !Unit Gaussian prior on all nuisance parameters
       if (Use_BBN) GetLogLikePost = GetLogLikePost + (CMB%ombh2 - 0.022)**2/(2*0.002**2) 
          !I'm using increased error bars here
   
       if (Use_CMB .or. Use_LSS) then
          if (HasCls) then
           acl = inCls
           error =0
          else
           call GetCls(CMB, Info, acl, error)
          end if
         if (error /= 0) then
          GetLogLikePost = logZero 
         else
          if (Use_CMB) GetLogLikePost = &
            CMBLnLike(acl, CMB%norm(norm_freq_ix:norm_freq_ix+num_freq_params-1),CMB%nuisance) + GetLogLikePost
          if (Use_mpk) GetLogLikePost = GetLogLikePost + LSSLnLike(CMB, Info%theory)
          if (Use_WeakLen) GetLogLikePost = GetLogLikePost + WeakLenLnLike(CMB, Info%theory)     
          if (Use_Lya) GetLogLikePost = GetLogLikePost +  LSS_Lyalike(CMB, Info%Theory)
          if ( GetLogLikePost >= logZero) then
            GetLogLikePost = logZero
          end if
         end if
         if (Use_SN .and. GetLogLikePost /= logZero ) then
            if (Info%Theory%SN_loglike /= 0) then
             GetLogLikePost = GetLogLikePost + Info%Theory%SN_loglike
            else
             GetLogLikePost = GetLogLikePost + SN_LnLike(CMB)
            end if
               !Assume computed only every time hard parameters change
         end if
         if (Use_BAO .and. GetLogLikePost /= logZero ) then
            if (Info%Theory%BAO_loglike /= 0) then
             GetLogLikePost = GetLogLikePost + Info%Theory%BAO_loglike
            else
             GetLogLikePost = GetLogLikePost + BAO_LnLike(CMB)
            end if
               !Assume computed only every time hard parameters change
         end if
         if (Use_HST .and. GetLogLikePost /= logZero) then
            if (Info%Theory%HST_loglike /= 0) then
             GetLogLikePost = GetLogLikePost + Info%Theory%HST_loglike
            else
             GetLogLikePost = GetLogLikePost + HST_LnLike(CMB)
            end if
           !!Old: GetLogLikePost = GetLogLikePost + (CMB%H0 - 72)**2/(2*8**2)  !HST 
         end if  
     
         else
           if (Use_SN) GetLogLikePost = GetLogLikePost + SN_LnLike(CMB)
           if (Use_BAO)then
                !From Jason Dosset           
                !JD had to change below so new BAO will work without CMB
                !previous way z_drag was not calculated without calling get_cls.
                if (RecomputeTransfers(CMB, Info%LastParams))  then
                    call CMBToCAMB(CMB, P)
                    call CAMBParams_Set(P)
                    call InitVars
                    Info%Theory%BAO_loglike = Bao_lnLike(CMB)
                    Info%LastParams = CMB
                end if
                GetLogLikePost = GetLogLikePost + Info%Theory%BAO_loglike
           end if
           if (Use_HST) GetLogLikePost = GetLogLikePost + HST_LnLike(CMB)
         end if
         
     
      if (Use_Clusters .and. GetLogLikePost /= LogZero) then
          GetLogLikePost = GetLogLikePost + &
                 (Info%Theory%Sigma_8-0.9)**2/(2*0.05**2)
          stop 'Write your cluster prior in calclike.f90 first!'
      end if

     if (GetLogLikePost /= LogZero) GetLogLikePost = GetLogLikePost/Temperature
   

    end if

  end function GetLogLikePost

end module CalcLike
